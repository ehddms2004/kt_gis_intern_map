/**
 * @author Dongeun Kim
 * @file main.js
 * @module RouteManager
 * @description 경로탐색, 안내를 위한 Manager
 */
var RouteManager = (function() {
    var instance;

    var LOG_HEAD = "[route.js]\n";

    /* API Setting */
    var API_URL = "https://gis.kt.com";
    var API_KEY = "BEARER df21e533e679ef05f19ab3e318085f629da782757fdc5c65d31bb6815687f18a61711750";

    /**
     * @description 경로를 탐색하기 위한 초기 setting 함수
     * @param {string} type 경로 탐색 종료 (driving or walking(미구현))
     * @param {array} callbackFuncs 경로 탐색 후 호출 될 callback 함수의 배열(혹은 함수)
     * @param {object} start 출발지 마커의 위치 { x, y }
     * @param {object} via1 경유지1 마커의 위치 { x, y }
     * @param {object} via2 경유지2 마커의 위치 { x, y }
     * @param {object} via3 경유지3 마커의 위치 { x, y }
     * @param {object} end 도착지 마커의 위치 { x, y }
     */
    var searchRoute = function(type, callbackFuncs, start, via1, via2, via3, end) {
        var params = makeParams(type, start, via1, via2, via3, end);
        console.log(LOG_HEAD + "searchRoute\n", params);
        callAPI(type, params, callbackFuncs);
    }

    /**
     * @description API 호출을 위하여 parameter를 생성하는 함수
     * @return {object} params에 필요한 요소를 추가한 object
     */
    var makeParams = function(type, start, via1, via2, via3, end) {
        var params = {};
        switch (type) {
            case 'driving':
                params.sx = (new olleh.maps.UTMK.valueOf(start)).x;
                params.sy = (new olleh.maps.UTMK.valueOf(start)).y;
                params.ex = (new olleh.maps.UTMK.valueOf(end)).x;
                params.ey = (new olleh.maps.UTMK.valueOf(end)).y;
                if (via1 != null) {
                    params.vx1 = (new olleh.maps.UTMK.valueOf(via1)).x;
                    params.vy1 = (new olleh.maps.UTMK.valueOf(via1)).y;
                }
                if (via2 != null) {
                    params.vx2 = (new olleh.maps.UTMK.valueOf(via2)).x;
                    params.vy2 = (new olleh.maps.UTMK.valueOf(via2)).y;
                }
                if (via3 != null) {
                    params.vx3 = (new olleh.maps.UTMK.valueOf(via3)).x;
                    params.vy3 = (new olleh.maps.UTMK.valueOf(via3)).x;
                }
                break;
        }
        return params;
    }

    /**
     * @description API 호출를 호출 한 뒤 결과를 callback 함수들에게 넘긴다.
     */
    function callAPI(type, params, callbackFuncs) {
        var url = API_URL;
        var headers = {
            'Authorization': API_KEY,
            'Accept': 'Application/json',
        };

        switch (type) {
            case 'driving':
                url += "/lbs/rp";
                break;
            case 'walking':
                url += "/route/walking/v1.0";
                break;
            default:
                break;
        }

        $.ajax({
            type: 'get',
            url: url,
            headers: headers,
            dataType: 'json',
            data: params,
            success: function(response) {
                console.log(LOG_HEAD + "callAPI\n", response);
                if (!Array.isArray(callbackFuncs)) {
                    callbackFuncs(response, params);
                } else {
                    for (var i = 0; i < callbackFuncs.length; i++) {
                        callbackFuncs[i](response, params);
                    }
                }
            },
            error: function(e) {
                console.log(e.responseText);
                alert("서버와의 연결에 실패하였습니다.");
            }
        });
    }

    /**
     * @description 경로 탐색 후 경로의 배열을 LatLng 타입 배열로 변환하여 반환하는 함수 => 경로내 탐색을 위하여 필요하다.
     * @see main.js setRouteSearchResult()
     * @param {JSON} response api call 후 response json object
     * @return {array} olleh.maps.LatLng object의 배열
     */
    var getLatLngPath = function(response) {
        var LatLngPath = [];
        var links = response.DATA.link;
        for (var i = 0; i < links.length; i++) {
            var link = links[i];
            for (var j = 0; j < link.vertex_cnt; j++) {
                var utmk = new olleh.maps.UTMK(link.vertex[j]);
                var latlng = new olleh.maps.LatLng.valueOf(utmk);
                LatLngPath.push({
                    lat: latlng.y,
                    lng: latlng.x
                });
            }
        }
        return LatLngPath;
    }

    function init(_map) {
        console.log(LOG_HEAD, "init");

        return {
            searchRoute: searchRoute,
            getLatLngPath: getLatLngPath,
        }
    }

    return {
        getInstance: function() {
            if (instance === undefined) {
                instance = init();
            }
            return instance;
        }
    }
})();
