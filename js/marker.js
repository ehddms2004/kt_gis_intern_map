/**
 * @author Dongeun Kim
 * @file main.js
 * @description 마커 생성, 컨트롤을 담당
 */
var MarkerManager = (function() {
    var instance;

    var map;

    /* 생성되는 마커를 담기위한 배열과 이름 지정을 위한 index */
    var markers = {};
    var markerIndex = 0;

    var routeMarkersName = {};

    var centerMarker; // 중심 표시 마커
    var tempMarker = null; //임시 마커

    var tooltips = {};

    var markerClusterer = null;

    var LOG_HEAD = "[marker.js]\n"

    /**
     * @description 마커를 입력받은 position으로 set
     * @param {object} position marker의 position object { x : # , y : #}
     * @returns {string} marker의 identifier : "marker_" + markerIndex
     */
    var setMarker = function(position) {
        var name = "marker_" + markerIndex++;
        var marker = new olleh.maps.overlay.Marker({
            position: position,
        })
        marker.setMap(map);

        markers[name] = marker;
        return name;
    }

    /**
     * @description 마커를 입력받은 image로 set
     * @param {object} position marker의 position object { x : # , y : #}
     * @param {string} url 설정하고자 하는 image의 url
     * @returns {string} marker의 identifier : "marker_" + markerIndex
     */
    var setImageMarker = function(url, position) {
        var name = "marker_" + markerIndex++;
        var marker = new olleh.maps.overlay.Marker({
            position: position,
            icon: {
                url: url,
                size: new olleh.maps.Size(40, 40),
                anchor: new olleh.maps.Point(22, 40)
            }
        })
        marker.setZIndex(100);
        marker.setMap(map);

        markers[name] = marker;
        return name;
    }

    /**
     * @description marker의 visible을 입력받은 state로 set, state 값이 정의되지 않았을 경우 현재 상태를 반전한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     * @param {bool} state marker의 상태
     * @example
     * (Managerobject).toggleMarker(name, true)
     * (Managerobject).toogleMarker(name)
     */
    var toggleMarker = function(name, state) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "toggleMarker\n", "Marker name " + name + " is wrong");
            return false;
        }
        if (state == undefined) {
            state = !(marker.isVisible());
        }
        marker.setVisible(state);
    }

    /**
     * @description 여러개의 marker들을 동시에 toggle
     * @param {array} names marker들의 이름(identifier) 배열
     * @param {bool} state marker의 상태
     */
    var toggleMarkers = function(names, state) {
        var length = names.length;
        for (var i = 0; i < names.length; i++) {
            var name = names[i];
            var marker = markers[name];
            if (marker === undefined) {
                console.log(LOG_HEAD + "toggleMarkers\n", "Marker name " + name + " is wrong");
                return false;
            }
            if (state == undefined) {
                state = !(marker.isVisible());
            }
            marker.setVisible(state);
        }
    }

    /**
     * @description marker의 위치로 map을 이동한다.
     * @param {string} name marker의 identifier
     */
    var moveToMarker = function(name) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "moveToMarker\n", "Marker name " + name + " is wrong");
            return false;
        }
        map.panTo(marker.getPosition());
    }

    /**
     * @description marker들의 위치를 전부 포함하는 위치로 map을 이동한다.
     * @param {array} names marker의 identifier들을 가진 array
     */
    var moveToMarkers = function(names) {
        var length = names.length;
        var position = markers[names[0]].getPosition();
        var bounds = new olleh.maps.Bounds(position, position);
        for (var i = 0; i < length; i++) {
            var marker = markers[names[i]];
            if (marker === undefined) {
                console.log(LOG_HEAD + "moveToMarkers\n", "Marker name " + name + " is wrong");
                return false;
            }
            position = marker.getPosition();
            bounds = bounds.union(position);
        }
        map.panToBounds(bounds);
    }

    /**
     * @description marker에 callback event를 binding 한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     * @param {string} event event명
     * @param {function} callback event가 발생시 실행할 함수
     * @example (Managerobject).eventBindMarker(name, "click", someFunc...);
     */
    var eventBindMarker = function(name, event, callback, params) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "eventBindMarker\n", "Marker name " + name + " is wrong");
            return false;
        }
        marker.onEvent(event, callback, params);
    }

    /**
     * @description marker의 위치를 return 한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     * @returns {object} marker의 position을 가지고 있는 object, x와 y속성을 가지고 있음
     */
    var getPositionMarker = function(name) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "getPositionMarker\n", "Marker name " + name + " is wrong");
            return false;
        }
        return marker.getPosition();
    }

    /**
     * @description marker의 위치를 set 한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     * @returns {object} marker의 position을 가지고 있는 object, x와 y속성을 가지고 있음
     */
    var setPositionMarker = function(name, position) {
        var marker = markers[name]
        if (marker === undefined) {
            console.log(LOG_HEAD + "setPositionMarker\n", "Marker name " + name + " is wrong");
            return false;
        }
        marker.setPosition(position)
    }

    /**
     * @description marker의 크기를 set 한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     * @param {object} size marker의 size를 가지고 있는 object, olleh.maps.Size()
     */
    var setMarkerSize = function(name, size) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "setMarkerSize\n", "Marker name " + name + " is wrong");
            return false;
        }
        var url = marker._icon.url;
        var option = {
            icon: {
                url: url,
                size: size
            }
        };
        marker.setOptions(option);
        console.log(marker._icon);
        marker.setMap(map);
    }

    /**
     * @description marker의 image를 set 한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     * @param {string} url  지정하고자 하는 image의 url
     */
    var setMarkerImage = function(name, url) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "setMarkerImage\n", "Marker name " + name + " is wrong");
            return false;
        }
        var size = marker._icon.size;
        var option = {
            icon: {
                url: url,
                size: size
            }
        };
        marker.setOptions(option);
        console.log(marker._icon);
        marker.setMap(map);
    }

    /**
     * @description marker의 caption을 set 한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     * @param {string} caption 캡션의 내용
     * @param {bool} visible 캡션의 의 visible 여부(true : visible , false : hidden)
     */
    var setMarkerCaption = function(name, caption, visible) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "setMarkerCaption\n", "Marker name " + name + " is wrong");
            return false;
        }
        var option = {
            caption: caption
        };
        if (visible == false) {
            marker._caption = null;
        }
        marker.setOptions(option);
        console.log(marker);
        marker.setMap(null);
        marker.setMap(map);
    }

    /**
     * @description marker의 z-index를 set 한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     * @param {int} value z-index의 값
     */
    var setMarkerZIndex = function(name, value) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "setMarkerZIndex\n", "Marker name " + name + " is wrong");
            return false;
        }
        if (value == null) {
            marker.setZIndex("auto");
        } else {
            marker.setZIndex(value);
        }
        //marker.setMap(null);
        marker.setMap(map);
    }

    /**
     * @description marker를 삭제한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     */
    var removeMarker = function(name) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "removeMarker\n", "Marker name " + name + " is wrong");
            return false;
        }
        var info = marker.infoWindow;
        if (info) {
            info.close();
        }
        removeTooltip(name);
        marker.setMap(null);
        delete markers[name];
    }

    /**
     * @description 여러개의 마커를 삭제한다.
     * @param {array} names marker의 identifier array : "marker_" + markerIndex
     */
    var removeMarkers = function(names) {
        for (var i = 0; i < names.length; i++) {
            var name = names[i];
            removeMarker(name);
        }
    }

    /**
     * @description 중심 표시 마커를 생성한다.
     * @param {object}position marker의 position을 가지고 있는 object, x와 y속성을 가지고 있음
     */
    var setCenterMarker = function(position) {
        centerMarker = new olleh.maps.overlay.Marker({
            position: position,
            icon: {
                url: "./images_ex/ic_center.png",
                size: new olleh.maps.Size(40, 40),
                anchor: new olleh.maps.Point(20, 20)
            },
        })

        centerMarker.setZIndex(100);
        centerMarker.setCursor('default');
        centerMarker.setMap(map);
        centerMarker.setShadow('./images_ex/ic_rotate_null.png');
        console.log('kde', centerMarker);
    }

    /**
     * @description 중심 표시 마커의 위치를 변경한다.
     * @param {object}position marker의 position을 가지고 있는 object, x와 y속성을 가지고 있음
     */
    var setCenterMarkerPosition = function(position) {
        centerMarker.setPosition(position)
    }

    /**
     * @description 중심 표시 마커를 toggle한다.
     * @param {state}state true == visible , false == hidden, undefined == 현재 상태 반전
     */
    var toggleCenterMarker = function(state) {
        if (state == undefined) {
            state = !(centerMarker.isVisible());
        }
        centerMarker.setVisible(state);
    }

    /**
     * @description 임시 표시 마커를 set한다.
     * @param {object}position marker의 position을 가지고 있는 object, x와 y속성을 가지고 있음
     */
    var setTempMarker = function(position) {
        if (tempMarker == null) {
            tempMarker = new olleh.maps.overlay.Marker({
                position: position,
                icon: {
                    url: "./images_ex/marker_red.png",
                    size: new olleh.maps.Size(40, 40),
                    anchor: new olleh.maps.Point(22, 40)
                }
            })
            tempMarker.setMap(map);
        } else {
            tempMarker.setPosition(position);
        }
    }

    /**
     * @description 임시 표시 마커의 위치를 변경한다.
     * @param {object} position marker의 position을 가지고 있는 object, x와 y속성을 가지고 있음
     */
    var setTempMarkerPosition = function(position) {
        tempMarker.setPosition(position)
    }

    /**
     * @description 임시 표시 마커를 toggle한다.
     * @param {state}state true == visible , false == hidden, undefined == 현재 상태 반전
     */
    var toggleTempMarker = function(state) {
        if (state == undefined) {
            state = !(tempMarker.isVisible());
        }
        tempMarker.setVisible(state);
    }

    /**
     * @description 임시 표시 마커의 위치로 지도를 pan한다.
     */
    var panToTempMarker = function() {
        if (tempMarker == null) {
            console.log(LOG_HEAD + "panToTempMarker\n", "tempMarker is null");
            return false;
        }
        map.panTo(tempMarker.getPosition());
    }

    /**
     * @description markers에 저장된 모든 마커를 제거한다.
     */
    var removeAllMarkers = function() {
        for (var name in markers) {
            removeMarker(name);
        }
    }

    /**
     * @description 마커에 info window를 set한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     * @param {string} content info window 안에 들어갈 내용, html tag도 가능하다.
     */
    var setInfoWindow = function(name, content) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "setInfoWindow\n", "Marker name " + name + " is wrong");
            return false;
        }
        var info = new olleh.maps.overlay.InfoWindow({
            position: marker.getPosition(),
            content: content,
        });
        info.setMap(map);
        info.open(map, marker);
        info.bringToFront();

        info.onEvent("close_click", function() {
            marker.infoWindow = null;
        });
        marker.infoWindow = info;
    }

    /**
     * @description info window를 toggle한다.
     * @param {state}state true == visible , false == hidden, undefined == 현재 상태 반전
     */
    var toggleInfoWindow = function(name, state) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "toggleInfoWindow\n", "Marker name " + name + " is wrong");
            return false;
        }
        var info = marker.infoWindow;
        if (state == undefined) {
            state = !(info.isVisible());
        }
        info.setVisible(state);
    }

    /**
     * @description info window에 callback event를 binding 한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     * @param {string} event event명(click, rightclick ...)
     * @param {function} callback event가 발생시 실행할 함수
     */
    var eventBindInfoWindow = function(name, event, callback) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "eventBindInfoWindow\n", "Marker name " + name + " is wrong");
            return false;
        }
        var info = marker.infoWindow;
        info.onEvent(event, callback);
    }

    /**
     * @description info window로 지도를 이동한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     */
    var moveToInfoWindow = function(name) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "moveToInfoWindow\n", "Marker name " + name + " is wrong");
            return false;
        }
        var info = marker.infoWindow;
        map.panTo(infoWindow.getPosition());
    }

    /**
     * @description info window의 위치를 반환한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     * @return {Object} info window의 position을 가지고 있는 object, x와 y속성을 가지고 있음
     */
    var getPositionInfoWindow = function(name) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "getPositionInfoWindow\n", "Marker name " + name + " is wrong");
            return false;
        }
        return marker.info.getPosition();
    }

    /**
     * @description info window를 삭제한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     */
    var removeInfoWindow = function(name) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "removeInfoWindow\n", "Marker name " + name + " is wrong");
            return false;
        }
        var info = marker.infoWindow;
        if (info) {
            info.close();
        }
        marker.infoWindow = null;
    }

    /**
     * @description api call 후 response에서 위치를 가져와서 경로 안내 마커를 표시한다.
     * @param {JSON} response api call 의 response
     */
    var setRouteMarkers = function(response) {
        var route = response.ROUTE.rg;
        routeMarkersName = {};
        nameArrForPan = [];
        var index = 0;

        for (var i = 0; i < route.length; i++) {
            var point = new olleh.maps.UTMK(route[i].x, route[i].y);
            var name = "route_" + index++;
            routeMarkersName[name] = setMarker(point);
            nameArrForPan.push(routeMarkersName[name]);
            toggleMarker(routeMarkersName[name], false);
        }
        console.log(LOG_HEAD + "setRouteMarkers\n", routeMarkersName);
        moveToMarkers(nameArrForPan);
        map.zoomOut();
        return routeMarkersName;
    }

    /**
     * @description 경로 안내 마커만을 삭제한다.
     * @param {string} name route marker의 identifier : "marker_" + markerIndex
     */
    var removeRouteMarker = function(name) {
        var routeMarker = markers[routeMarkersName[name]];
        if (routeMarker === undefined) {
            console.log(LOG_HEAD + "toggleTooltip\n", "routeMarker name " + name + " is wrong");
            return false;
        }
        delete routeMarkersName[name];
        removeMarker(routeMarker);
    }

    /**
     * @description 경로 안내 마커전체를 삭제한다.
     */
    var removeAllRouteMarkers = function() {
        for (var name in routeMarkersName) {
            removeRouteMarker(name);
        }
    }

    var getRouteMarkersName = function() {
        return routeMarkersName;
    }

    /**
     * @description 툴팁(마커 아래 설명 글)을 생성한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     * @param {string} content tooltip에 들어갈 내용
     */
    var setTooltip = function(name, content) {
        var marker = markers[name];
        var tooltip = new olleh.maps.overlay.Tooltip({
            content: content,
            map: map,
            position: marker.getPosition()
        });
        tooltips[name] = tooltip;
        if (tooltip._body === null) {
            return false;
        }
        //툴팁을 가운데 정렬
        var half = tooltip._body.size.width / 2 * (-1);
        tooltip.setPixelOffset(new olleh.maps.Point(half, 5));
    }

    /**
     * @description tooltip을 toggle한다.
     * @param {state}state true == visible , false == hidden, undefined == 현재 상태 반전
     */
    var toggleTooltip = function(name, state) {
        var tooltip = tooltips[name];

        if (tooltip === undefined) {
            console.log(LOG_HEAD + "toggleTooltip\n", "tooltip name " + name + " is wrong");
            return false;
        }
        if (state == undefined) {
            state = !(tooltip.isVisible());
        }
        tooltip.setVisible(state);
    }

    /**
     * @description tooltip을 제거한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     */
    var removeTooltip = function(name) {
        var tooltip = tooltips[name];
        if (tooltip === undefined) {
            console.log(LOG_HEAD + "removeTooltip\n", "tooltip name " + name + " is wrong");
            return false;
        }
        tooltip.setMap(null);
        delete tooltips[name];
    }

    var removeAllTooltips = function() {
        for (var name in tooltips) {
            removeTooltip(name);
        }
    }

    /**
     * @description 마커를 marker cluster에 추가한다.
     * @param {string} name marker의 identifier : "marker_" + markerIndex
     */
    var addMarkerInCluster = function(name) {
        var marker = markers[name];
        if (marker === undefined) {
            console.log(LOG_HEAD + "addMarkerInCluster\n", "marker name " + name + " is wrong");
            return false;
        }
        markerClusterer.add(marker);
    }

    var clearMarkerCluster = function() {
        markerClusterer.clear();
    }

    function init(_map) {

        console.log(LOG_HEAD, "init");

        markerClusterer = new olleh.maps.overlay.MarkerClusterer();
        markerClusterer.setMap(_map);

        map = _map;

        return {
            setMarker: setMarker,
            setImageMarker: setImageMarker,
            setPositionMarker: setPositionMarker,
            setMarkerSize: setMarkerSize,
            setMarkerImage: setMarkerImage,
            setMarkerCaption: setMarkerCaption,
            setMarkerZIndex: setMarkerZIndex,
            setCenterMarker: setCenterMarker,
            setCenterMarkerPosition: setCenterMarkerPosition,
            toggleCenterMarker: toggleCenterMarker,
            setTempMarker: setTempMarker,
            setTempMarkerPosition: setTempMarkerPosition,
            panToTempMarker: panToTempMarker,
            toggleTempMarker: toggleTempMarker,
            toggleMarker: toggleMarker,
            toggleMarkers: toggleMarkers,
            eventBindMarker: eventBindMarker,
            moveToMarker: moveToMarker,
            moveToMarkers: moveToMarkers,
            getPositionMarker: getPositionMarker,
            removeMarker: removeMarker,
            removeMarkers: removeMarkers,
            removeAllMarkers: removeAllMarkers,
            setRouteMarkers: setRouteMarkers,
            removeRouteMarker: removeRouteMarker,
            removeAllRouteMarkers: removeAllRouteMarkers,
            getRouteMarkersName: getRouteMarkersName,
            setTooltip: setTooltip,
            toggleTooltip: toggleTooltip,
            removeTooltip: removeTooltip,
            removeAllTooltips: removeAllTooltips,

            setInfoWindow: setInfoWindow,
            toggleInfoWindow: toggleInfoWindow,
            eventBindInfoWindow: eventBindInfoWindow,
            moveToInfoWindow: moveToInfoWindow,
            getPositionInfoWindow: getPositionInfoWindow,

            addMarkerInCluster: addMarkerInCluster,
            clearMarkerCluster: clearMarkerCluster,
        }
    }

    return {
        getInstance: function(map) {
            if (instance === undefined) {
                instance = init(map);
            }
            return instance;
        }
    }

})();
