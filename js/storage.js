/**
 * @author Dongeun Kim
 * @file storage.js
 * @module StorageManager
 * @description Local storage에 정보를 저장하기 위한 module
 */
var StorageManager = (function() {
    var instance;

    var LOG_HEAD = "[storage.js]\n"

    var MAX_KEYWORD = 10;
    var keyArray = {};

    var setItem = function(key, value) {
        localStorage.setItem(key, value);
    }
    var getItem = function(key) {
        return localStorage.getItem(key);
    }
    var removeItem = function(key) {
        localStorage.removeItem(key);
    }

    /**
     * @description 정보를 저장하기 위한 함수
     * @param {string} setName 저장소의 이름
     * @param {array} itemSet 저장 될 정보가 배열로 전달된다.
     */
    var setItemSet = function(setName, itemSet) {
        //var index = 0;
        for (var i = 0; i < itemSet.length; i++) {
            var key = setName + "_" + i;
            keyArray[key] = true;
            setItem(key, itemSet[i]);
        }

        console.log(keyArray);
    }

    /**
     * @description 정보를 불러오기 위한 함수
     * @param {string} setName 저장소의 이름
     */
    var getItemSet = function(setName) {
        var resultArr = [];
        var length = Object.keys(keyArray).length;
        for (var i = 0; i < length; i++) {
            var key = setName + "_" + i;
            if (keyArray[key] == true) {
                resultArr.push(getItem(key));
            } else {
                break;
            }
        }
        return resultArr;
    }

    /**
     * @description 검색기록 정보를 저장하기 위한 함수
     * @param {string} keyword 저장하고자 하는 검색어
     */
    var addHistory = function(keyword) {
        var keywordArr = [];
        var jsonItem = getItem("history");
        if (jsonItem === null) {
            keywordArr.push(keyword);
        } else {
            keywordArr = JSON.parse(jsonItem);
            //중복 검사
            for (var i = 0; i < keywordArr.length; i++) {
                if (keywordArr[i] == keyword) {
                    return false;
                }
            }
            //검색어 저장 개수(MAX_KEYWORD)를 초과하면 앞의 검색어부터 삭제한다. (Like Queue)
            if (keywordArr.length >= MAX_KEYWORD) {
                keywordArr.splice(0, 1);
            }
            keywordArr.push(keyword);
        }
        setItem("history", JSON.stringify(keywordArr));
        console.log(LOG_HEAD + "addHistory\n", keywordArr);
    }

    /**
     * @description 검색기록 정보를 가져오기 위한 함수
     * @return {array} 검색 기록을 배열화
     */
    var getHistory = function() {
        var keywordArr = [];
        var jsonItem = getItem("history");
        if (jsonItem != null) {
            keywordArr = JSON.parse(jsonItem);
        }
        return keywordArr;
    }

    /**
     * @description UI control setting 정보를 저장하기 위한 함수
     * @param {string} id 저장하고자 하는 setting id
     * @param {bool} state 저장하고자 하는 setting id의 state 값
     */
    var saveUIControl = function(id, state) {
        var uiArr = [];
        var jsonItem = getItem("UIControl");
        if (jsonItem === null) {
            uiArr.push({
                'id': id,
                'state': state
            });
        } else {
            uiArr = JSON.parse(jsonItem);
            var find = false;
            var index = 0;
            for (var i = 0; i < uiArr.length; i++) {
                if (uiArr[i].id == id) {
                    find = true;
                    index = i;
                }
            }
            if (find) {
                uiArr[index].state = state;
            } else {
                uiArr.push({
                    'id': id,
                    'state': state
                });
            }
        }
        setItem("UIControl", JSON.stringify(uiArr));
        console.log(LOG_HEAD + "saveUIControl\n", uiArr);
    }

    /**
    * @description UI control setting 정보반환해주는 함수.
    * @return {array} UI control setting 값을 배열화

    */
    var getUIControl = function() {
        var uiArr = [];
        var jsonItem = getItem("UIControl");
        if (jsonItem != null) {
            uiArr = JSON.parse(jsonItem);
        }
        return uiArr;
    }

    function init(_map) {
        console.log(LOG_HEAD, "init");

        return {
            setItem: setItem,
            getItem: getItem,
            removeItem: removeItem,
            setItemSet: setItemSet,
            getItemSet: getItemSet,
            addHistory: addHistory,
            getHistory: getHistory,
            saveUIControl: saveUIControl,
            getUIControl: getUIControl,
        }
    }

    return {
        getInstance: function() {
            if (instance === undefined) {
                instance = init();
            }
            return instance;
        }
    }

})();
