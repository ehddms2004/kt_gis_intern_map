/**
 * @author Dongeun Kim
 * @file weather.js
 * @module WeatherManager
 * @description 날씨 정보를 가져오기 위한 모듈 정의, skt api 사용
 */
"use strict";
var WeatherManager = (function() {
    var instance;

    var LOG_HEAD = "weather.js\n";

    /* API information */
    var API_URL = "https://api2.sktelecom.com/weather/current";
    var API_KEY = "eb25bb61-617a-49e9-b34f-8efd84745f2d";

    /**
     * @description 입력 받은 위치의 날씨를 알아오는 api를 호출한다.
     */
    var getWeather = function(lat, lon, callbackFuncs) {
        var params = {
            lat: lat,
            lon: lon
        }
        callAPI('minutely', params, callbackFuncs);
    }

    function callAPI(type, params, callbackFuncs) {
        var url = API_URL;
        var headers = {
            'appKey': API_KEY,
            'Content-Type': '[{"key":"Content-Type","value":"application/json; charset=UTF-8","description":""}]',
            'Accept': 'Application/json',
        };

        switch (type) {
            case 'minutely':
                url += "/minutely";
                break;
            case 'hourly':
                url += "/hourly";
                break;
            default:
                break;
        }

        $.ajax({
            type: 'get',
            url: url,
            headers: headers,
            dataType: 'json',
            data: params,
            success: function(response) {
                console.log(LOG_HEAD + "callAPI\n", response);
                if (!Array.isArray(callbackFuncs)) {
                    callbackFuncs(response, params);
                } else {
                    for (var i = 0; i < callbackFuncs.length; i++) {
                        callbackFuncs[i](response, params);
                    }
                }
            },
            error: function(e) {
                console.log(e.responseText);
                alert("서버와의 연결에 실패하였습니다.");
            }
        });
    }

    function init() {
        return {
            getWeather: getWeather,
        }
    }
    return {
        getInstance: function() {
            if (instance === undefined) {
                console.log(LOG_HEAD, "init");
                instance = init();
            }
            return instance;
        }
    }
})();
