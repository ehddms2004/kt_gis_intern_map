/**
 * @author Dongeun Kim
 * @file main.js
 * @description 전체적인 동작 실행을 담당
 */
$(function() {
    var mapOpts = {
        center: new olleh.maps.LatLng(37.484515, 127.034051),
        panControl: false,
    }
    map = new olleh.maps.Map(document.getElementById('map'), mapOpts);

    LOG_HEAD = "[main.js]\n";

    /* 마우스 오른쪽 메뉴 변수 */
    var contextMenuDom = document.getElementById("context-menus");

    /* 마우스 클릭 리스너를 초기 실행시킨다. */
    function customContextinit() {
        rightMouseListener();
        leftMouseListener();
    }

    /* 마우스 왼클릭 감지 */
    function leftMouseListener() {
        document.addEventListener("click", function(e) {
            toggleOnOff(0);
        })
    }

    /* 마우스 우클릭 감지 */
    function rightMouseListener() {
        document.addEventListener("contextmenu", function(e) {
            event.preventDefault();
            toggleOnOff(1);
            showMenu(e.x, e.y);
        });
    }

    /* 마우스 메뉴 on & off */
    function toggleOnOff(num) {
        num === 1 ? contextMenuDom.classList.add("active") : contextMenuDom.classList.remove("active");
    }

    /* 마우스 클릭한 지점에서 메뉴 보여줌 */
    function showMenu(x, y) {
        console.log(contextMenuDom);
        contextMenuDom.style.top = y + "px";
        contextMenuDom.style.left = x + "px";

    }
    customContextinit();

    viaArray = [0, 0, 0];

    autoCompleteData = {};

    /* Manager instance 생성 */
    searchManager = SearchManager.getInstance();
    markerManager = MarkerManager.getInstance(map);
    vectorManager = VectorManager.getInstance(map);
    uiManager = UIManager.getInstance(map);
    storageManager = StorageManager.getInstance();
    routeManager = RouteManager.getInstance();
    weatherManager = WeatherManager.getInstance();

    /* 마커를 담기 위한 List 초기화 */
    placeMarkerList = {};
    addressMarkerList = {};
    addressMarkerListForAll = {};
    categoryMarkerList = {};
    searchInRouteMarkerList = [];

    routeSearchPath = [];

    /* 경로 탐색 시 출발/경유/도착지를 표시할 마커 초기화 */
    startMarker = {
        markerName: '',
        markerPosition: null
    };
    via1Marker = {
        markerName: '',
        markerPosition: null
    };
    via2Marker = {
        markerName: '',
        markerPosition: null
    };
    via3Marker = {
        markerName: '',
        markerPosition: null
    };
    endMarker = {
        markerName: '',
        markerPosition: null
    };

    /* 우클릭을 실행했을 경우 좌표를 저장하고, 마커와 면적 vector를 저장하기 위한 변수 */
    rightClickCoord = null;
    rightClickMarkerName = '';
    rightClickPolygonName = '';

    /* 시작 시 중심좌표 셋팅, 날씨 초기화 */
    var center = new olleh.maps.LatLng.valueOf(map.getCenter());
    searchManager.reverseGeocode(center, mapCenterChanged);
    if($('#btn_ui_weather').is(':checked')){
        weatherManager.getWeather(center.y, center.x, uiManager.setWeather);
    }
    markerManager.setCenterMarker(center);

    /* 지도 이동 시 이벤트 등록 */
    map.onEvent('dragend', function(event, payload) {
        var center = new olleh.maps.LatLng.valueOf(map.getCenter());
        console.log(LOG_HEAD + "mapCenterChanged\n", center);
        searchManager.reverseGeocode(center, mapCenterChanged);
        if($('#btn_ui_weather').is(':checked')){
            weatherManager.getWeather(center.y, center.x, uiManager.setWeather);
        }
    });
    map.onEvent('drag', function(event, payload) {
        var center = new olleh.maps.LatLng.valueOf(map.getCenter());
        markerManager.setCenterMarkerPosition(center);
    });
    map.onEvent('zoom_changed', function(event, payload) {
        var center = new olleh.maps.LatLng.valueOf(map.getCenter());
        console.log(LOG_HEAD + "mapCenterChanged\n", center);
        searchManager.reverseGeocode(center, mapCenterChanged);
        if($('#btn_ui_weather').is(':checked')){
            weatherManager.getWeather(center.y, center.x, uiManager.setWeather);
        }
        markerManager.setCenterMarkerPosition(center);
    });
    map.onEvent('rightclick', function(event, payload) {
        console.log(LOG_HEAD + "rightclick\n", event.getCoord());
        rightClickCoord = event.getCoord();
    });

    /* 저장된 UI셋팅을 불러와서 초기 지도의 UI를 셋팅한다. */
    var uiArr = storageManager.getUIControl();
    for(var i = 0 ; i < uiArr.length ; i++){
      var id = uiArr[i].id;
      var state = uiArr[i].state;
      uiManager.setUIControl(id,state);
      uiManager.UIControl(id,state);
    }


    /* 시작시 검색기록 셋팅 */
    uiManager.setHistoryList();

    /* 검색 input 변경 감지 */
    $('.input_act').on('input', function() {
        //console.log($(this).val());
        var inpVal = $(this).val();
        if (inpVal == '' || inpVal === undefined) {
            console.log(LOG_HEAD + "input_act", "no input");
            uiManager.openHistoryList($(this));
        } else {
            uiManager.openSuggestList($(this));
            searchManager.autoComplete($(this).val(), uiManager.setAutocompleteResult);
            printAdminMsg($(this).val());
        }
    });
    $(".input_act").focus(function() {
        uiManager.openHistoryList($(this));
    });
    $(".input_act").focusout(function() {
        window.setTimeout(function() {
            $(".wrap_history").css({
                "display": "none"
            });
            $(".wrap_route_history").css({
                "display": "none"
            });
            $(".wrap_suggest").css({
                "display": "none"
            });
            $(".wrap_route_suggest").css({
                "display": "none"
            });
        }, 150);
    });

    /* UI Setting toggle 버튼 클릭 감지 */
    $(".ui_setting_toggle").change(function() {
        uiManager.UIControl($(this).attr('id'), this.checked);
    });

    /* Autocomplete list 클릭 이벤트 바인딩 */
    uiManager.bindAddressAutocomplete();

    /* 검색기록 클릭 바인딩 */
    uiManager.bindHistoryItems();

    /* 경유지 드래그앤 드랍 정렬 기능 등록 */
    $('.sortable').sortable();

    /* 백그라운드를 클릭했을 경우 네비게이션이 들어가도록 한다. */
    $(".bg_black").click(function() {
        console.log("h");
        if ($(".bg_black").css("display") == "block") {
            $("#main_drawer").stop().animate({
                left: '-405px'
            }, 250);
            $(".bg_black").css({
                "display": "none"
            });
        }
    });

    /* index.html 에서 버튼 클릭 동작 등록 */

    $("#btn_nav_close").click(function() {
        $("#main_drawer").stop().animate({
            left: '-405px'
        }, 250);
        $(".bg_black").css({
            "display": "none"
        });
    });


    $('#btn_main_drawer_admin').click(function() {
        $('#admin_console').css({
            "display": ""
        });
    });
    $('#btn_admin_close').click(function() {
        $('#admin_console').css({
            "display": "none"
        });
    });

    $("#btn_road").click(function() {
        uiManager.onClickBtnRoad();
    });

    //길찾기 종료
    $("#btn_route_drawer_close").click(function() {
        clearRouteSearchResult();
        clearRouteGuideResult();
        uiManager.closeRouteSearch();
        uiManager.closeSearchInRoute();
    });

    //길찾기 화살표 버튼 액션 => 서랍 액션
    $("#btn_route_drawer_arrow").click(function() {
        var isClosed = ($("#btn_route_drawer_arrow").attr('class') != 'btn_route_drawer_arrow_close'); //버튼이 close상태이므로 열려있는 상태
        if (isClosed) {
            $("#route_drawer").stop().animate({
                left: '10px'
            }, 250);
            $("#btn_route_drawer_arrow").attr('class', 'btn_route_drawer_arrow_close');
        } else {
            $("#route_drawer").stop().animate({
                left: '-380px'
            }, 250);
            $("#btn_route_drawer_arrow").attr('class', 'btn_route_drawer_arrow_open');
        }
    });

    //주소 검색 화살표 버튼 액션 => 서랍 액션
    $("#btn_address_drawer_arrow").click(function() {
        var isClosed = ($("#btn_address_drawer_arrow").attr('class') != 'btn_address_drawer_arrow_close'); //버튼이 close상태이므로 열려있는 상태
        if (isClosed) {
            $(".geo_box").stop().animate({
                left: '20px'
            }, 250);
            $("#btn_address_drawer_arrow").attr('class', 'btn_address_drawer_arrow_close');
        } else {
            $(".geo_box").stop().animate({
                left: '-370px'
            }, 250);
            $("#btn_address_drawer_arrow").attr('class', 'btn_address_drawer_arrow_open');
        }
    });

    //경로내 검색
    $('#btn_search_in_route').click(function(){
        var className = $('#btn_search_in_route').attr('class');
        if(className == 'btn_search'){
            onClickSearchBtn('searchInRoute')
        }else{
            markerManager.removeMarkers(searchInRouteMarkerList);
            searchInRouteMarkerList = [];
            $('#btn_search_in_route').attr('class','btn_search');
            $('#inp_search_in_route').val('');
        }
    });

    //테마 검색 화면
    $(".btn_theme").click(function() {
        $(".wrap_facilty").show();
    });

    //테마 검색 닫기
    $(".close_facilty").click(function() {
        $(".wrap_facilty").hide();
        markerManager.clearMarkerCluster();
        for (key in categoryMarkerList) {
            markerManager.removeMarker(categoryMarkerList[key]);
        }
    });

    //날씨 버튼(자세한 날씨)
    $("#btn_weather").click(function(){
        uiManager.toggleWeather();
    });


    //검색 결과 초기화
    $(".btn_clear").click(function() {
        console.log(LOG_HEAD + "clear\n");
        clearSearchResult();
    });
});

/* main.js init 종료 */

/**
 * @description 우클릭 실행 시 적절한 동작 실행
 */
function contextMenu(index){
    switch(index){
        case 0:
            var utmk = new olleh.maps.LatLng.valueOf(rightClickCoord);
            searchManager.reverseGeocode(utmk, showInfo);
            break;
        case 1:
            var utmk = new olleh.maps.LatLng.valueOf(rightClickCoord);
            rightClickRouteType = "start";
            searchManager.reverseGeocode(utmk, setRightClickRouteSearch);
            break;
        case 2:
            var utmk = new olleh.maps.LatLng.valueOf(rightClickCoord);
            rightClickRouteType = "end";
            searchManager.reverseGeocode(utmk, setRightClickRouteSearch);
            break;

    }
}

/**
 * @description 우클릭 실행 후 여기 주소 보기 했을 경우 api 호출 후 정보 info window에 출력 함수
 */
function showInfo(response, params){
    markerManager.removeMarker(rightClickMarkerName);
    rightClickMarkerName = markerManager.setMarker(rightClickCoord);

    var poi = response.residentialAddress[0].parcelAddress[0];
    var geographicInformation = poi.geographicInformation;
    var shape = geographicInformation.shape;
    if(shape.type == "MultiPolygon"){
        vectorManager.removeMultipolygon(rightClickPolygonName);
        var path = shape.coordinates[0][0];
        console.log(LOG_HEAD + "coordinates", path);
        rightClickPolygonName = vectorManager.drawMultipolygon(path);
    }

    var tmpl =
    '<div style="font-family: dotum, arial, sans-serif;font-size: 11px; font-weight: bold;margin-bottom: 5px;">#{fullAddress}</div>' +
    '<table style="border-spacing: 2px; border: 0px"><tbody>' +
    '<tr><td style="color:#767676;padding-right:12px">위도</td>' +
    '<td><span>#{lat}</span></td></tr>' +
    '<tr><td style="color:#767676;padding-right:12px">경도</td>' +
    '<td><span>#{lng}</span></td></tr>' +
    '</tbody></table>';

    var data = {
         fullAddress: poi.fullAddress,
         lat: geographicInformation.point.lat.toFixed(4),
         lng: geographicInformation.point.lng.toFixed(4),
    };

    var content = olleh.maps.util.applyTemplate(tmpl,data);
    markerManager.setInfoWindow(rightClickMarkerName,content);
    markerManager.eventBindInfoWindow(rightClickMarkerName ,'close_click', closeRightClickInfo)
    console.log("kde",poi);
}

function closeRightClickInfo(){
    markerManager.removeMarker(rightClickMarkerName);
    vectorManager.removeMultipolygon(rightClickPolygonName);
}

function openUISetting(){
    $("#main_drawer").stop().animate({
        left: '0px'
    }, 250);
    $(".bg_black").css({
        "display": "block"
    });
}

function startCategorySearch(code) {
    var placeSearchOpt = {
        code: code,
        pageIndex: 0,
        currentBtnIndex: 0,
        'lat': olleh.maps.LatLng.valueOf(map.getCenter()).y,
        'lng': olleh.maps.LatLng.valueOf(map.getCenter()).x,
    };
    searchManager.setPlaceSearchOpt(placeSearchOpt);
    searchCategoryPlace(searchManager.getPlaceSearchOpt());
}

function panToCenter() {
    if (navigator.geolocation) { // GPS를 지원하면
        navigator.geolocation.getCurrentPosition(function(position) {
            var position = new olleh.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.panTo(position);
        }, function(error) {
            console.error(error);
        }, {
            enableHighAccuracy: false,
            maximumAge: 0,
            timeout: Infinity
        });
    } else {
        alert('GPS를 지원하지 않습니다');
    }
}

function removeHistory() {
    storageManager.removeItem('history');
    uiManager.setHistoryList();
}

//경유지 추가 버튼
function addVia() {
    if ($("#btn_route_search_add_via").attr('class') == "route_search_opt_btn") {
        console.log(LOG_HEAD + "add via", viaArray);
        for (var i = 0; i < 3; i++) {
            if (viaArray[i] == 0) {
                $("#route_via_" + (i + 1).toString()).css({
                    "display": ""
                });
                viaArray[i] = 1;
                break;
            }
        }
    }
}

/**
 *@brief 맵의 중심좌표가 변경되었을 때 호출된다.
 *@see map.onEvent
 */
var mapCenterChanged = function(response, params) {
    console.log(LOG_HEAD + "mapCenterChanged\n", response);
    var residentialAddress = response.residentialAddress;
    var parcelAddress = residentialAddress[0].parcelAddress;
    console.log(parcelAddress[0].siDo + " " + parcelAddress[0].siGunGu);
    $("#center_address").text(parcelAddress[0].siDo + " " + parcelAddress[0].siGunGu);
}

/**
 * @description 검색 기록을 모두 초기화 한다.
 * @see setRouteSearch()
 * @see $("#btn_road").click()
 * @see $(".btn_clear").click()
 */
function clearSearchResult() {
    markerManager.removeAllMarkers();
    placeMarkerList = {};
    addressMarkerList = {};
    addressMarkerListForAll = {};

    searchManager.setSearchOpt(null);
    searchManager.setPlaceSearchOpt(null);

    //검색 창 비우기
    $("#inp_address_search").val('');
    //기존 검색 결과 삭제
    uiManager.removeSearchResultItems("search_place_result");
    uiManager.removeSearchResultItems("search_all_place_result");
    uiManager.removeSearchResultItems("search_all_address_result");
    uiManager.removeSearchResultItems("search_address_result");
    //검색 결과창 끔
    uiManager.closeAddressSearchResult();
}

/**
 * @description 경로 안내 결과를 초기화 한다.
 * @see clearRouteSearchResult()
 * @see $("#btn_route_drawer_close").click()
 */
function clearRouteGuideResult() {
    uiManager.removeSearchResultItems("search_route_result");
}

/**
 * @description 경로 안내 벡터, 마커를 지도에서 지운다.
 * @see index.html 모두 초기화 버튼
 * @see $("#btn_route_drawer_close").click()
 */
function clearRouteSearchResult() {
    //마커 초기화
    markerManager.removeAllMarkers();
    //벡터 초기화
    vectorManager.removeAllRoutes();
    //경로내 검색 닫기
    uiManager.closeSearchInRoute();
    //경로 검색창 toggle
    uiManager.toggleRouteSearchInput(true);

    startMarker = {
        markerName: '',
        markerPosition: null
    };
    via1Marker = {
        markerName: '',
        markerPosition: null
    };
    via2Marker = {
        markerName: '',
        markerPosition: null
    };
    via3Marker = {
        markerName: '',
        markerPosition: null
    };
    endMarker = {
        markerName: '',
        markerPosition: null
    };

    //기존 검색 결과 삭제
    uiManager.removeSearchResultItems("search_route_place_result");
    //경로안내 삭제
    clearRouteGuideResult();
    //경유지 추가 버튼 재설정
    $('#btn_route_search_add_via').removeClass('btn_disable');
    //검색 버튼 재설정
    checkRoute();

    //경유지 삭제
    removeVia(1);
    removeVia(2);
    removeVia(3);

    //검색 창 비우기
    $("#inp_route_start_search").val('');
    $("#inp_route_via_1_search").val('');
    $("#inp_route_via_2_search").val('');
    $("#inp_route_via_3_search").val('');
    $("#inp_route_end_search").val('');
}

/**
 * @description 전체, 장소, 주소 결과 탭을 이동한다.
 * @see $('.tab_panel')
 */
function changeSearchResultTab(index) {
    markerManager.removeAllMarkers();
    placeMarkerList = {};
    addressMarkerList = {};
    addressMarkerListForAll = {};

    uiManager.changeSearchResultTab(index);
}

/**
 * @description 장소 검색에서 관련도순, 거리순 으로 정렬되었을 때 실행된다.
 * @see index.html
 */
function onClickChangeSearchOpt(sortBy) {
    uiManager.toggleSearchOptBtn(sortBy);
    var placeSearchOpt = searchManager.getPlaceSearchOpt();
    placeSearchOpt['sortBy'] = sortBy;
    placeSearchOpt['pageIndex'] = 0;
    placeSearchOpt['currentBtnIndex'] = 0;
    placeSearchOpt['lat'] = olleh.maps.LatLng.valueOf(map.getCenter()).y;
    placeSearchOpt['lng'] = olleh.maps.LatLng.valueOf(map.getCenter()).x;
    console.log('kde', placeSearchOpt);
    searchPlace(placeSearchOpt);
}

/**
 * @description 길찾기 화면에서 검색어를 삭제 버튼을 눌렀을 때 실행된다.
 * @see index.html
 */
function clearSearchInput(type) {
    switch (type) {
        case "start":
            $('#inp_route_start_search').val('');
            markerManager.removeMarker(startMarker.markerName);
            startMarker = {
                markerName: '',
                markerPosition: null
            };
            break;
        case "end":
            $('#inp_route_end_search').val('');
            markerManager.removeMarker(endMarker.markerName);
            endMarker = {
                markerName: '',
                markerPosition: null
            };
            break;
    }
    checkRoute();
}

/**
 * @description 경유지를 삭제할 때 실행된다.
 * @see index.html
 */
function removeVia(index) {
    $("#route_via_" + index).css({
        "display": "none"
    });
    switch (index) {
        case 1:
            markerManager.removeMarker(via1Marker.markerName);
            $('#inp_route_via_1_search').val('');
            via1Marker = {
                markerName: '',
                markerPosition: null
            };
            break;
        case 2:
            markerManager.removeMarker(via2Marker.markerName);
            $('#inp_route_via_2_search').val('');
            via2Marker = {
                markerName: '',
                markerPosition: null
            };
            break;
        case 3:
            markerManager.removeMarker(via3Marker.markerName);
            $('#inp_route_via_3_search').val('');
            via3Marker = {
                markerName: '',
                markerPosition: null
            };
            break;
    }
    viaArray[index - 1] = 0;
    checkRoute();
}

/**
 *@description 검색 결과 리스트 내에서 출발, 도착 버튼을 클릭할 시 호출
 */
function onClickStartEndButton(poi, type) {
    console.log(LOG_HEAD + "onClickStartEndButton\n", poi);
    console.log(LOG_HEAD + "onClickStartEndButton\n", type);
}

/**
 *@description 출발, 도착지가 정해져있는지 확인. 만약 되어있다면 길찾기 버튼을 비활성화
 */
function checkRoute() {
    // startMarker = {markerName : '' , markerPosition : null};
    if (startMarker.markerName != '' && endMarker.markerName != '') {
        $("#btn_route_search_start").removeClass('btn_disable');
    } else {
        $("#btn_route_search_start").addClass('btn_disable');
    }
}

/**
 *@description routeManager의 searchRoute를 호출 => callback의 결과로 아래의 함수들이 실행된다.
 *@param setRouteSearchResult()
 *@param vectorManager.drawRoute()
 *@param markerManager.setRouteMarkers()
 */
function startRouteSearch() {
    var viaList = $('#via_list').children();
    var child0 = ($(viaList).children()[0].id).split('_')[2] - 1;
    var child1 = ($(viaList).children()[1].id).split('_')[2] - 1;
    var child2 = ($(viaList).children()[2].id).split('_')[2] - 1;

    var viaArr = [];
    viaArr.push(via1Marker);
    viaArr.push(via2Marker);
    viaArr.push(via3Marker);

    if ($("#btn_route_search_start").attr('class') == "route_search_opt_btn") {
        routeManager.searchRoute("driving",
            [setRouteSearchResult, vectorManager.drawRoute, markerManager.setRouteMarkers],
            startMarker.markerPosition,
            viaArr[child0].markerPosition,
            viaArr[child1].markerPosition,
            viaArr[child2].markerPosition,
            endMarker.markerPosition
        );
    }
    $('#btn_route_search_start').addClass('btn_disable');
    $('#btn_route_search_add_via').addClass('btn_disable');
}

/**
 *@class rgInfo
 *@description 경로 안내의 정보를 담기위한 class
 */
var RgInfo = (function() {
    var node_name;
    var dir_name;
    var x, y;
    var nextdist;
    var link_idx;
    var tspdinfo;
    var speed;
    var type;

    function RgInfo(rgItem) {
        this.node_name = rgItem.node_name;
        this.dir_name = rgItem.dir_name;
        this.x = rgItem.x;
        this.y = rgItem.y;
        this.nextdist = rgItem.nextdist;
        this.link_idx = rgItem.link_idx;
        this.tspdinfo = rgItem.tspdinfo;
        this.speed = rgItem.speed;
        this.type = rgItem.type;
    }
    RgInfo.prototype.getNodeName = function() {
        return this.node_name;
    };
    RgInfo.prototype.getDirName = function() {
        var dirArr = this.dir_name.split('|');
        var dirName = '';
        dirName += dirArr[0];
        for (var i = 1; i < dirArr.length; i++) {
            dirName += ', ';
            dirName += dirArr[i];
        }
        return dirName;
    };
    RgInfo.prototype.getNextDist = function() {
        return this.nextdist;
    }
    RgInfo.prototype.getTspdInfo = function() {
        return this.tspdinfo;
    }
    RgInfo.prototype.getSpeed = function() {
        return this.speed;
    }
    RgInfo.prototype.getType = function() {
        var types = {
            0: '안내없음',
            1: '직진',
            2: '1시 방향 우회전',
            3: '2시 방향 우회전',
            4: '우회전',
            5: '4시 방향 우회전',
            6: '5시 방향 우회전',
            7: '7시 방향 우회전',
            8: '8시 방향 우회전',
            9: '좌회전',
            10: '10시 방향 좌회전',
            11: '11시 방향 좌회전',
            12: '직진 방향에 고가도로 진입',
            13: '오른쪽 방향에 고가도로 진입',
            14: '왼쪽 방향에 고가도로 진입',
            15: '지하차도 진입',
            16: '오른쪽 방향에 고가도로 옆도로 진입',
            17: '왼쪽 방향에 고가도로 옆도로 진입',
            18: '오른쪽 방향에 지하차도 옆도로 진입',
            19: '왼쪽 방향에 지하차도 옆도로 진입',
            20: '오른쪽 도로 진입',
            21: '왼쪽 도로 진입',
            22: '직진 방향에 고속도로 진입',
            23: '오른쪽 방향에 고속도로 진입',
            24: '왼쪽 방향에 고속도로 진입',
            25: '직진 방향에 도시고속도로 진입',
            26: '오른쪽 방향에 도시고속도로 진입',
            27: '왼쪽 방향에 도시고속도로 진입',
            28: '오른쪽 방향에 고속도로 출구로 진입',
            29: '왼쪽 방향에 고속도로 출구로 진입',
            30: '오른쪽 방향에 도시고속도로 출구로 진입',
            31: '왼쪽 방향에 도시고속도로 출구로 진입',
            32: '분기점에서 직진',
            33: '분기점에서 오른쪽',
            34: '분기점에서 왼쪽',
            35: 'U 턴',
            36: '무발성 직진',
            37: '터널 진입',
            38: '없음',
            39: '없음',
            40: '로터리에서 1시 방향',
            41: '로터리에서 2시 방향',
            42: '로터리에서 3시 방향',
            43: '로터리에서 4시 방향',
            44: '로터리에서 5시 방향',
            45: '로터리에서 6시 방향',
            46: '로터리에서 7시 방향',
            47: '로터리에서 8시 방향',
            48: '로터리에서 9시 방향',
            49: '로터리에서 10시 방향',
            50: '로터리에서 11시 방향',
            51: '로터리에서 12시 방향',
            52: 'IC',
            53: 'JC',
            54: '톨게이트 진입',
            55: '휴게소',
            999: '출발지',
            1000: '경유지',
            1001: '목적지'
        };
        return types[this.type];
    }
    //경로 안내 이미지 아이콘을 지정한다.
    RgInfo.prototype.getGuideImg = function() {
        var guideImg = {
            0: 'rotate_null',
            1: 'rotate_a',
            2: 'rotate_f',
            3: 'rotate_a',
            4: 'rotate_c',
            5: 'rotate_h',
            6: 'rotate_h',
            7: 'rotate_g',
            8: 'rotate_g',
            9: 'rotate_b',
            10: 'rotate_e',
            11: 'rotate_e',
            12: 'rotate_i',
            13: 'rotate_i',
            14: 'rotate_i',
            15: 'rotate_l',
            16: 'rotate_k',
            17: 'rotate_k',
            18: 'rotate_k',
            19: 'rotate_k',
            20: 'rotate_null',
            21: 'rotate_null',
            22: 'rotate_i',
            23: 'rotate_i',
            24: 'rotate_i',
            25: 'rotate_i',
            26: 'rotate_i',
            27: 'rotate_i',
            28: 'rotate_i',
            29: 'rotate_i',
            30: 'rotate_i',
            31: 'rotate_i',
            32: 'rotate_a',
            33: 'rotate_f',
            34: 'rotate_e',
            35: 'rotate_d',
            36: 'rotate_a',
            37: 'rotate_l',
            38: 'rotate_null',
            39: 'rotate_null',
            40: 'rotate_null',
            41: 'rotate_null',
            42: 'rotate_null',
            43: 'rotate_null',
            44: 'rotate_null',
            45: 'rotate_null',
            46: 'rotate_null',
            47: 'rotate_null',
            48: 'rotate_null',
            49: 'rotate_null',
            50: 'rotate_null',
            51: 'rotate_null',
            52: 'rotate_null',
            53: 'rotate_null',
            54: 'rotate_null',
            55: 'rotate_null',
            999: 'flag_red_small',
            1000: 'flag_blue_small',
            1001: 'flag_green_small'
        };
        return guideImg[this.type];
    }
    return RgInfo;
})();

/**
 * @description 경로 탐색 수행 후 결과를 표현하는 함수
 * @see setRouteSearchResult()
 */
var setRouteSearchResult = function(response) {
    uiManager.toggleRouteSearchInput(false);
    //길찾기 경로 저장 - 경로내 탐색 때문
    routeSearchPath = routeManager.getLatLngPath(response);
    console.log('kde', routeSearchPath);

    //경로내 검색 열기
    uiManager.openSearchInRoute();

    //길찾기용 검색 창 닫기
    $("#route_search_result_container").css('display', 'none');
    //경로 안내 결과
    $("#search_route_result_container").css('display', '');

    var route = response.ROUTE.rg;
    var rgArr = [];

    for (var i = 0; i < route.length; i++) {
        var rgInfo = new RgInfo(route[i]);
        rgArr.push(rgInfo);
        console.log(LOG_HEAD + "rgInfo\n", rgInfo.getType());
        uiManager.addRouteSearchResultItem("search_route_result", i + 1, rgInfo);
    }
    uiManager.bindRouteGuideItems("search_route_result", response);

    console.log(LOG_HEAD + "setRouteSearchResult\n", rgArr);
}

/**
 * @description 검색 버튼이 클릭되면 실행되는 함수
 * @see index.html
 */
function onClickSearchBtn(type) {
    //자동완성창 닫기
    $(".wrap_suggest").css({
        "display": "none"
    });
    $(".wrap_route_suggest").css({
        "display": "none"
    });
    //장소 검색을 관련순으로 변경
    uiManager.toggleSearchOptBtn("RELEVANCE");
    var keyword;
    if (type == 'address') {
        keyword = $('#inp_address_search').val();
        //키워드, 검색 조건(거리,정확도) 등등
        if (keyword == '') {
            return false;
        }
        searchOpt = {
            keyword: keyword,
            pageIndex: 0,
            currentBtnIndex: 0,
            sortBy: "RELEVANCE",
            searchType: "address",
            'lat': olleh.maps.LatLng.valueOf(map.getCenter()).y,
            'lng': olleh.maps.LatLng.valueOf(map.getCenter()).x,
        };
        searchManager.setSearchOpt(searchOpt);
        placeSearchOpt = { //장소 검색만을 위한 opt => pagination때문에
          keyword: keyword,
          pageIndex: 0,
          currentBtnIndex: 0,
          sortBy: "RELEVANCE",
          searchType: "address",
          'lat': olleh.maps.LatLng.valueOf(map.getCenter()).y,
          'lng': olleh.maps.LatLng.valueOf(map.getCenter()).x,
        }
        searchManager.setPlaceSearchOpt(placeSearchOpt);
        //기존 마커 삭제
        markerManager.removeAllMarkers();
        placeMarkerList = {};
        addressMarkerList = {};
        addressMarkerListForAll = {};

        //기존 검색 결과 삭제
        uiManager.removeSearchResultItems("search_place_result");
        uiManager.removeSearchResultItems("search_all_place_result");
        uiManager.removeSearchResultItems("search_all_address_result");
        uiManager.removeSearchResultItems("search_address_result");
        //검색 결과창 켜 주고 tab은 전체 검색으로 변경 해줌
        uiManager.openAddressSearchResult();
        console.log('kde',searchManager.getSearchOpt());
        searchAll(searchManager.getSearchOpt());
    } else if (type == 'searchInRoute') {
        keyword = $('#inp_search_in_route').val();

        var searchOpt = {
            keyword: keyword,
            route: routeSearchPath,
            distance: 500,
        }
        searchManager.setSearchOpt(searchOpt);
        searchInRoute(searchManager.getSearchOpt());
    } else {
        switch (type) {
            case 'start':
                keyword = $('#inp_route_start_search').val();
                break;
            case 'via_1':
                keyword = $('#inp_route_via_1_search').val();
                break;
            case 'via_2':
                keyword = $('#inp_route_via_2_search').val();
                break;
            case 'via_3':
                keyword = $('#inp_route_via_3_search').val();
                break;
            case 'end':
                keyword = $('#inp_route_end_search').val();
                break;
        }
        //검색어가 없을 경우 실행하지 않는다.
        if (keyword == '') {
            return false;
        }
        var placeSearchOpt = {
            keyword: keyword,
            pageIndex: 0,
            currentBtnIndex: 0,
            sortBy: "RELEVANCE",
            'lat': olleh.maps.LatLng.valueOf(map.getCenter()).y,
            'lng': olleh.maps.LatLng.valueOf(map.getCenter()).x,
            searchType: type,
        }
        searchManager.setPlaceSearchOpt(placeSearchOpt);
        searchRoutePlace(searchManager.getPlaceSearchOpt());
    }
    //save search keyword
    storageManager.addHistory(keyword);
    uiManager.setHistoryList();
}

/**
 *@description 주소, 장소 동시에 검색하고 표시한다.
 *@see setAllSearchResult()
 */
function searchAll(searchOpt) {
    uiManager.toggleAjaxLayer(true);
    $('#strong_address_search_keyword').text("'" + searchOpt.keyword + "'"); //검색어 표시 ex) 광화문 검색결과 (1건)
    searchManager.searchPOI(searchOpt.keyword, setAllSearchResult, searchOpt);
}

/**
 *@description 장소를 검색하고 표시한다.
 *@see setPlaceSearchResult()
 */
function searchPlace(placeSearchOpt) { //pagination이 발생했을 경우에도 호출된다.
    uiManager.toggleAjaxLayer(true);
    searchManager.searchPOI(placeSearchOpt.keyword, setPlaceSearchResult, placeSearchOpt);
}

/**
 *@description 카테고리로 검색하고 표시한다.
 *@see setCategorySearchResult()
 */
function searchCategoryPlace(placeSearchOpt) {
    //uiManager.toggleAjaxLayer(true);
    searchManager.searchCategoryPlace(setCategorySearchResult, placeSearchOpt);
}

/**
 *@description 경로 탐색 후 경로내에서 검색한다.
 *@see setSearchInRouteResult()
 */
function searchInRoute(searchOpt) {
    searchManager.searchInRoute(setSearchInRouteResult, searchOpt);
}

/**
 *@description 경로내 탐색 결과를 지도에 표시한다.
 *@see searchInRoute()
 */
function setSearchInRouteResult(response, params) {
    $('#btn_search_in_route').attr('class','btn_search_in_route_close');
    console.log('setSearchInRouteResult', response);
    var pois = response.pois;
    for (var i = 0; i < pois.length; i++) {
        var poi = pois[i];
        var position = new olleh.maps.LatLng(poi.point.lat, poi.point.lng);
        var name = markerManager.setMarker(position);
        searchInRouteMarkerList.push(name);
        var params = {
            name: name,
            poi: poi
        }
        markerManager.eventBindMarker(name, 'click', setCategoryInfo, params);
    }
}

/**
 *@description 카테고리 탐색 결과를 지도에 표시한다.
 *@see searchCategoryPlace()
 */
function setCategorySearchResult(response, params) {
    //기존 카테고리 마커 삭제
    markerManager.clearMarkerCluster();
    for (key in categoryMarkerList) {
        markerManager.removeMarker(categoryMarkerList[key]);
    }
    var pois = response.pois;
    console.log("kde", params['theme.code']);
    var code = params['theme.code'];
    var ico = '';
    switch (code) {
        case 'TM_001':
            ico = 'ico_gas';
            break;
        case 'TM_002':
            ico = 'ico_lpg';
            break;
        case 'TM_003':
            ico = 'ico_parking';
            break;
        case 'TM_004':
            ico = 'ico_clean';
            break;
        case 'TM_005':
            ico = 'ico_restaurant';
            break;
        case 'TM_006':
            ico = 'ico_hotel';
            break;
        case 'TM_007':
            ico = 'ico_bank';
            break;
        case 'TM_008':
            ico = 'ico_mart';
            break;
        case 'TM_009':
            ico = 'ico_gro';
            break;
        case 'TM_010':
            ico = 'ico_hospital';
            break;
        case 'TM_011':
            ico = 'ico_pham';
            break;
        case 'TM_012':
            ico = 'ico_toilet';
            break;
    }
    //마커 생성 후 marker cluster에 추가
    for (var i = 0; i < pois.length; i++) {
        var poi = pois[i];
        var position = new olleh.maps.LatLng(poi.point.lat, poi.point.lng);
        var name = markerManager.setImageMarker('./images/' + ico + '.jpg', position);

        categoryMarkerList[poi.id] = name;
        markerManager.setMarkerSize(name, new olleh.maps.Size(25, 25));
        var params = {
            name: name,
            poi: poi
        }
        markerManager.addMarkerInCluster(name);
        markerManager.eventBindMarker(name, 'click', setCategoryInfo, params);
    }
}

/**
 *@description 카테고리 탐색 결과를 info window에 등록한다.
 *@see setCategorySearchResult()
 */
function setCategoryInfo() {
    var name = this.name;
    var poi = this.poi;

    var tmpl =
        '<div style="font-family: dotum, arial, sans-serif;font-size: 13px; font-weight: bold;margin-bottom: 5px;">#{title}</div>' +
        '<table style="border-spacing: 2px; border: 0px"><tbody>' +
        '<tr><td style="color:#767676;padding-right:12px">분류</td>' +
        '<td><span>#{category}</span></td></tr>' +
        '<tr><td style="color:#767676;padding-right:12px">신주소</td>' +
        '<td><span>#{roadAddr}</span></td></tr>' +
        '<tr><td style="color:#767676;padding-right:12px">지번</td>' +
        '<td style=""><span>#{addr}</span></td></tr>' +
        '<tr><td style="color:#767676;padding-right:12px">전화번호</td>' +
        '<td><span>#{tel}</span></td>' +
        '</tr></tbody></table>';

    var data = {
        title: poi.name + ' ' + (poi.subName || '') + (poi.branch && '(' + poi.branch + ')'),
        category: poi.category.masterName + " > " + poi.category.middleName,
        roadAddr: poi.address.siDo + " " + poi.address.siGunGu + (poi.address.street && " " + poi.address.street || "") + (poi.address.streetNumber && " " + poi.address.streetNumber || ""),
        addr: (poi.address.eupMyeon && " " + poi.address.eupMyeon || "") + (poi.address.eupMyeonDong && " " + poi.address.eupMyeonDong || "") + (poi.address.ri && " " + poi.address.ri || "") + (poi.address.houseNumber && ' ' + poi.address.houseNumber || ''),
        tel: (poi.phones && poi.phones.representation || ""),
    };

    var content = olleh.maps.util.applyTemplate(tmpl, data);
    markerManager.setInfoWindow(name, content);
}

/**
 *@description 주소를 검색 한 후 결과를 표시한다.
 *@param setAddressSearchResult()
 */
function searchAddress(searchOpt) {
    uiManager.toggleAjaxLayer(true);
    $('#strong_address_search_keyword2').text("'" + searchOpt.keyword + "'"); //검색어 표시 ex) 서울 검색결과 (1건)
    searchManager.searchPOI(searchOpt.keyword, setAddressSearchResult, searchOpt);
}

/**
 *@description 경로 검색을 위한 주소를 검색하고 표시한다.
 *@param setAddressSearchResult()
 */
function searchRoutePlace(placeSearchOpt) { //pagination이 발생했을 경우에도 호출된다.
    uiManager.toggleAjaxLayer(true);
    searchManager.searchPOI(placeSearchOpt.keyword, setRoutePlaceSearchResult, placeSearchOpt);
}

/**
 *@description 출발/도착 버튼이 눌러졌을 때 실행되는 함수 / 출발 도착 마커를 생성한다
 */
function setRouteSearch(poi, type) {
    clearSearchResult(); //검색결과 삭제 후 창 닫기

    uiManager.openRouteSearch(); //길찾기 열기
    switch (type) {
        case "start":
            $('#inp_route_start_search').val(poi.name + " " + (poi.subName || "") + (poi.branch && "(" + poi.branch + ")"));
            if (startMarker.markerName != '') {
                markerManager.removeMarker(startMarker.markerName);
            }
            var position = new olleh.maps.LatLng(poi.point.lat, poi.point.lng);
            var markerName = markerManager.setImageMarker("./images_ex/ic_flag_red.png", position);
            startMarker = {
                markerName: markerName,
                markerPosition: position
            };
            break;
        case "end":
            $('#inp_route_end_search').val(poi.name + " " + (poi.subName || "") + (poi.branch && "(" + poi.branch + ")"));
            if (endMarker.markerName != '') {
                markerManager.removeMarker(endMarker.markerName);
            }
            var position = new olleh.maps.LatLng(poi.point.lat, poi.point.lng);
            var markerName = markerManager.setImageMarker("./images_ex/ic_flag_green.png", position);
            endMarker = {
                markerName: markerName,
                markerPosition: position
            };
            break;
    }
    console.log("kde", poi);
    checkRoute();
}

/**
 *@description 오른클릭 메뉴에서 출발/도착 버튼이 눌러졌을 때 실행되는 함수 / 출발 도착 마커를 생성한다
 */
function setRightClickRouteSearch(response, params){
    console.log('kde',$('.wrap_searchbox').css('display'));
    if($('.wrap_searchbox').css('display') != 'none'){
      clearSearchResult(); //검색결과 삭제 후 창 닫기
      uiManager.openRouteSearch(); //길찾기 열기
    }

    var poi = response.residentialAddress[0].parcelAddress[0];
    var geographicInformation = poi.geographicInformation;

    switch (rightClickRouteType) {
        case "start":
            $('#inp_route_start_search').val(poi.fullAddress);
            if (startMarker.markerName != '') {
                markerManager.removeMarker(startMarker.markerName);
            }
            var position = new olleh.maps.LatLng(geographicInformation.point.lat, geographicInformation.point.lng);
            var markerName = markerManager.setImageMarker("./images_ex/ic_flag_red.png", position);
            startMarker = {
                markerName: markerName,
                markerPosition: position
            };
            break;
        case "end":
            $('#inp_route_end_search').val(poi.fullAddress);
            if (endMarker.markerName != '') {
                markerManager.removeMarker(endMarker.markerName);
            }
            var position = new olleh.maps.LatLng(geographicInformation.point.lat, geographicInformation.point.lng);
            var markerName = markerManager.setImageMarker("./images_ex/ic_flag_green.png", position);
            endMarker = {
                markerName: markerName,
                markerPosition: position
            };
            break;
    }
    console.log("kde", poi);
    checkRoute();
}

/**
 *@description 검색이 최초 실행되었을 때 장소/주소 검색 결과를 모두 표시한다.
 *@see searchAll()
 */
function setAllSearchResult(response, params) {
    uiManager.toggleAjaxLayer(false); //로딩창 닫기
    uiManager.removeSearchResultItems("search_all_place_result");
    uiManager.removeSearchResultItems("search_all_address_result");
    $("#address_tab_all").animate({
        scrollTop: 0
    }, 0);

    var residentialAddress = response.residentialAddress;
    $('#all_address_search_count').text(residentialAddress.length);
    $('#all_place_search_count').text(response.numberOfPois);

    //주소 마커 생성 - 파란 마커
    for (var i = 0; i < response.residentialAddress.length; i++) {
        var parcelAddress = response.residentialAddress[i].parcelAddress[0];
        var geographicInformation = parcelAddress.geographicInformation;
        var position = new olleh.maps.LatLng(geographicInformation.point.lat, geographicInformation.point.lng);
        console.log(LOG_HEAD, position);
        addressMarkerListForAll[parcelAddress.badmId] = markerManager.setImageMarker("./images_ex/marker_blue.png", position);
    }
    //장소 마커 생성 - 빨간 마커
    for (var i = 0; i < response.pois.length; i++) {
        var poi = response.pois[i];
        var position = new olleh.maps.LatLng(poi.point.lat, poi.point.lng);
        console.log(LOG_HEAD, position);
        placeMarkerList[poi.id] = markerManager.setImageMarker("./images_ex/marker_red.png", position);
        var params = {
            name: placeMarkerList[poi.id],
            poi: poi
        }
        markerManager.eventBindMarker(placeMarkerList[poi.id], 'click', setCategoryInfo, params);
    }
    //마커 생성 끝

    //주소 검색 결과 리스트 업데이트
    for (var i = 0; i < residentialAddress.length; i++) {
        var parcelAddress = residentialAddress[i].parcelAddress;
        uiManager.addAddressSearchResultItem("search_all_address_result", i + 1, parcelAddress);
    }
    //주소 결과 목록 event binding
    uiManager.bindAddressSearchResultItems("search_all_address_result", response);

    //장소 검색 결과 리스트 업데이트
    for (var i = 0; i < response.pois.length; i++) {
        var poi = response.pois[i];
        uiManager.addPlaceSearchResultItem("search_all_place_result", i + 1, poi, "place"); //장소검색결과 업데이트
    }
    //장소 결과 목록 event binding
    uiManager.bindPlaceSearchResultItems("search_all_place_result", response, "place");

    //마커를 전부 볼 수 있는 비율로 지도 이동
    var markerNames = [];
    for (key in placeMarkerList) {
        markerNames.push(placeMarkerList[key]);
    }
    for (key in addressMarkerListForAll) {
        markerNames.push(addressMarkerListForAll[key]);
    }
    console.log(LOG_HEAD + "markerNames\n", markerNames);
    markerManager.moveToMarkers(markerNames);
}

/**
 * @description 장소 탭으로 변경되었을 때 장소 검색 결과를 모두 표시한다.
 * @see searchPlace()
 */
function setPlaceSearchResult(response, params) {
    uiManager.toggleAjaxLayer(false);
    uiManager.removeSearchResultItems("search_place_result");
    $("#search_place_result_container").animate({
        scrollTop: 0
    }, 0);
    //마커 초기화
    for (key in placeMarkerList) {
        markerManager.removeMarker(placeMarkerList[key]);
    }
    placeMarkerList = {};

    //Pagination 생성
    var placePagination = searchManager.newPaginationInstance();;
    if (params.start == 0) {
        $('#strong_place_search_count').text(response.numberOfPois);
        placePagination.setDomObj($('#pagination_place'));
        placePagination.setSearchFunc(searchPlace);
        placePagination.setSearchOpt(searchManager.getPlaceSearchOpt());
        placePagination.bindBtnClicked();
    }
    placePagination.setNumberOfPois(response.numberOfPois);

    //검색 결과 리스트 표시
    for (var i = 0; i < response.pois.length; i++) {
        var poi = response.pois[i];
        uiManager.addPlaceSearchResultItem("search_place_result", i + 1, poi, "place"); //장소검색결과 업데이트
        var position = new olleh.maps.LatLng(poi.point.lat, poi.point.lng);
        console.log(LOG_HEAD, position);
        placeMarkerList[poi.id] = markerManager.setImageMarker("./images_ex/marker_red.png", position);
        var params = {
            name: placeMarkerList[poi.id],
            poi: poi
        }
        markerManager.eventBindMarker(placeMarkerList[poi.id], 'click', setCategoryInfo, params);
    }
    console.log(LOG_HEAD, placeMarkerList);

    //검색 결과 목록 event binding
    uiManager.bindPlaceSearchResultItems("search_place_result", response, "place");

    //마커를 전부 볼 수 있는 비율로 지도 이동
    var markerNames = [];
    for (key in placeMarkerList) {
        markerNames.push(placeMarkerList[key]);
    }
    console.log(LOG_HEAD, markerNames);
    markerManager.moveToMarkers(markerNames);
}

/**
 * @description 주소 탭으로 변경되었을 때 장소 검색 결과를 모두 표시한다.
 * @see searchAddress()
 */
function setAddressSearchResult(response, params) {
    uiManager.toggleAjaxLayer(false);
    uiManager.removeSearchResultItems("search_address_result");
    $("#search_place_result_container").animate({
        scrollTop: 0
    }, 0);

    var residentialAddress = response.residentialAddress;
    $('#address_search_count').text(residentialAddress.length);

    for (key in addressMarkerList) {
        markerManager.removeMarker(addressMarkerList[key]);
    }
    addressMarkerList = {};

    //주소 마커 생성 - 파란색
    for (var i = 0; i < response.residentialAddress.length; i++) {
        var parcelAddress = response.residentialAddress[i].parcelAddress[0];
        var geographicInformation = parcelAddress.geographicInformation;
        var position = new olleh.maps.LatLng(geographicInformation.point.lat, geographicInformation.point.lng);
        console.log(LOG_HEAD, position);
        addressMarkerListForAll[parcelAddress.badmId] = markerManager.setImageMarker("./images_ex/marker_blue.png", position);
    }
    //마커 생성 끝

    //주소 검색 결과 리스트 업데이트
    for (var i = 0; i < residentialAddress.length; i++) {
        var parcelAddress = residentialAddress[i].parcelAddress;
        uiManager.addAddressSearchResultItem("search_address_result", i + 1, parcelAddress);
    }
    //주소 검색 결과 event binding
    uiManager.bindAddressSearchResultItems("search_address_result", response);

    //마커를 전부 볼 수 있는 비율로 지도 이동
    var markerNames = [];
    for (key in addressMarkerList) {
        markerNames.push(addressMarkerList[key]);
    }
    console.log(LOG_HEAD, markerNames);
    markerManager.moveToMarkers(markerNames);
}

/**
 * @description 경로 탐색을 위한 장소 검색 결과를 모두 표시한다.
 * @see searchRoutePlace()
 */
function setRoutePlaceSearchResult(response, params) {
    uiManager.toggleAjaxLayer(false);
    uiManager.removeSearchResultItems("search_route_place_result");

    //검색 결과 scroll을 최상단으로
    $("#search_route_place_result_container").animate({
        scrollTop: 0
    }, 0);

    //Pagination 생성
    var routePlacePagination = searchManager.newPaginationInstance();
    if (params.start == 0) {
        routePlacePagination.setDomObj($('#pagination_route_place'));
        routePlacePagination.setSearchFunc(searchRoutePlace);
        routePlacePagination.setSearchOpt(searchManager.getPlaceSearchOpt());
        routePlacePagination.bindBtnClicked();
    }
    routePlacePagination.setNumberOfPois(response.numberOfPois);

    //검색 결과를 lsit에 출력
    for (var i = 0; i < response.pois.length; i++) {
        var poi = response.pois[i];
        uiManager.addPlaceSearchResultItem("search_route_place_result", i + 1, poi, "route"); //장소검색결과 업데이트
    }
    console.log(LOG_HEAD + "searchType\n", params.searchType);
    uiManager.bindRoutePlaceSearchResultItems("search_route_place_result", response, params.searchType);
}
