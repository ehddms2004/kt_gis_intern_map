/**
* @author Dongeun Kim.
* @module VectorManager
* @description Route Search 를 한 이후 호출되어 map에 경로를 그리기 위한 함수
* @file vector.js
* @example VectorManager 2018.07.23
 - How to use.

	1. Load script in HTML
    <script type='text/javascript' src='js/vector.js'></script>

	2. Usage in javascript
    - make manager instance
    vectorManager = VectorManager.getVectorInstance(map);

    - call a function in manager
    vectorManager.removeRouteById(id);

    - call drawMultipolygon
    input path type = [[0,1],[0,1],[0,1],[0,1],[0,1] ,,, ] (latLng type)
*/
"use strict";
var VectorManager = (function() {
    var instance;

    var map;

    var LOG_HEAD = "vector.js\n";

    var nodes;
    var lineColor = 'blue';
    var routes = {};
    var index = 0;
    var polygons = {};

    /**
     * @description 경로 안내 정보를 vector로 그리기 위한 함수
     * @param {JSON} response routeManager에서 API call의 response
     * @param {object} params routeManager에서 API call의 params
     * @return {string} route의 identifier를 반환한다 : route_ + index
     */
    var drawRoute = function(response, params) {
        console.log(LOG_HEAD + "drawRouteByNodes\n", response);
        var path = getPath(response.DATA.link);

        console.log(LOG_HEAD + "getPath\n", path);
        var polylineDot = new olleh.maps.vector.Polyline({
            path: path,
            strokeColor: 'black',
            strokeOpacity: 1,
            strokeWeight: 2,
            strokeStyle: 'dot'
        });
        var polylineLine = new olleh.maps.vector.Polyline({
            path: path,
            strokeColor: lineColor,
            strokeOpacity: 0.5,
            strokeWeight: 6,
            strokeStyle: 'solid'
        });

        setMouseoverEvent(polylineLine);

        polylineDot.setMap(map);
        polylineLine.setMap(map);

        var name = "route_" + index++;
        routes[name] = {
            polylineDot: polylineDot,
            polylineLine: polylineLine
        };

        return name;
    };

    /**
     * @description 경로 안내 vector를 삭제하기 위한 함수
     * @param {string} name route의 identifier : route_ + index
     */
    var removeRoute = function(name) {
        var route = routes[name];
        if (route === undefined) {
            console.log(LOG_HEAD + "removeRoute\n", "route name " + name + " is wrong");
            return false;
        }
        var dot = route.polylineDot;
        var line = route.polylineLine;
        dot.setMap(null);
        line.setMap(null);

        delete routes[name];
    }

    /**
     * @description 모든 경로 안내 vector를 삭제하기 위한 함수
     */
    var removeAllRoutes = function() {
        for (var name in routes) {
            removeRoute(name);
        }
    };

    /**
     * @description 경로 안내 response에서 path를 가져오기 위한 함수
     * @param {JSON} links json response에서 link 정보를 가져온다.
     */
    function getPath(links) {
        var path = new olleh.maps.Path();
        for (var i = 0; i < links.length; i++) {
            var link = links[i];
            for (var j = 0; j < link.vertex_cnt; j++) {
                var utmk = new olleh.maps.UTMK(link.vertex[j]);
                path.push(new olleh.maps.LatLng.valueOf(utmk));
            }
        }
        return path;
    }

    /**
     * @description vector에 mouseover event를 binding 한다
     * @param {object} element event를 binding할 object
     */
    var setMouseoverEvent = function(element) {
        element.onEvent('mouseover', function(event, payload) {
            element.setOptions({
                strokeOpacity: 0.9
            });
        });

        element.onEvent('mouseout', function(event, payload) {
            element.setOptions({
                strokeOpacity: 0.5
            });
        });
    };

    /**
     * @description multipolygon을 그린다.
     * @param {array} paths multipolygon의 꼭지점 : [[0,1],[0,1],[0,1],[0,1],[0,1] ,,, ]
     * @param {object} options multipolygon을 그릴때 option
     * @return {string} multipolygon의 identifier를 반환한다 : polygon_ + index
     */
    var drawMultipolygon = function(paths, options) {
        var UTMKpaths = [];
        for (var i = 0; i < paths.length; i++) {
            var latlng = new olleh.maps.LatLng(paths[i][1], paths[i][0]);
            UTMKpaths.push(latlng.asDefault());
        }

        var path = new olleh.maps.Path(UTMKpaths);

        console.log(path);
        var opts = {
            paths: path
        };
        if (options == null) {
            console.log("no options for multipolygon");
            opts['strokeColor'] = 'blue';
            opts['strokeOpacity'] = 1;
            opts['strokeWeight'] = 3;
            opts['fillColor'] = 'blue';
            opts['fillOpacity'] = 0.5;
        }
        console.log(opts);

        var polygon = new olleh.maps.vector.Polygon(opts);

        console.log(polygons);
        polygon.setMap(map);

        var name = "polygon_" + index++;
        polygons[name] = polygon;

        return name;
    };

    /**
     * @description multipolygon을 삭제한다.
     * @param {string} name multipolygon의 identifier : polygon_ + index
     */
    var removeMultipolygon = function(name) {
        var polygon = polygons[name];
        if (polygon === undefined) {
            console.log(LOG_HEAD + "removePolygon\n", "polygon name " + name + " is wrong");
            return false;
        }
        polygon.setMap(null);

        delete polygons[name];
    };

    /**
     * @description 모든 multipolygon을 삭제한다.
     */
    var removeAllMultipolygons = function() {
        for (var name in polygons) {
            removeMultipolygon(name);
        }
    };

    function init(_map) {
        map = _map;

        return {
            drawRoute: drawRoute,
            removeRoute: removeRoute,
            removeAllRoutes: removeAllRoutes,

            drawMultipolygon: drawMultipolygon,
            removeMultipolygon: removeMultipolygon,
            removeAllMultipolygons: removeAllMultipolygons,
        }
    }
    return {
        getInstance: function(map) {
            if (instance === undefined) {
                console.log(LOG_HEAD, "init");
                instance = init(map);
            }
            return instance;
        }
    }
})();
