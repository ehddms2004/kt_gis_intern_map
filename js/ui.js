/**
 * @author Dongeun Kim
 * @file ui.js
 * @module UIManager
 * @description UI control을 위한 module, Dom object 추가, event binding 등을 실행한다.
 */
var UIManager = (function() {
    var instance;

    var map;

    var LOG_HEAD = "[ui.js]\n"

    var openAddressSearchResult = function() {
        $(".wrap_searchboxLy").css({
            "display": "block",
            "position": "absolute",
            "top": "33px",
            "bottom": "0px",
            "border": "1px solid #728aa2;"
        });
        $(".geo_box").css({
            "bottom": "0px",
        });
        $(".wrap_searchbox").css({
            "display": "block"
        });
        $(".btn_open03").css({
            "display": ""
        });
        $("#btn_road").attr('class', 'btn_search_close');

        $('#search_tab_all').attr("class", "on");
        $('#search_tab_place').attr("class", "");
        $('#search_tab_address').attr("class", "");
        $('#address_tab_all').css({
            'display': ''
        });
        $('#address_tab_place').css({
            'display': 'none'
        });
        $('#address_tab_address').css({
            'display': 'none'
        });
    }

    /**
     * @description 검색 버튼 옆의 길찾기 버튼을 클릭 했을때 실행되는 함수, 동작이 닫기/길찾기 두개이므로 함수로 제작
     */
    var onClickBtnRoad = function() {
        var address = $('#btn_ui_search').is(':checked');
        var route = $('#btn_ui_routeSearch').is(':checked');
        console.log(address + " " + route);

        if (address && route) { //둘다 on => 정상동작
            var isBtnRoute = ($("#btn_road").attr('class') == 'btn_road'); //길찾기 버튼이면
            console.log(isBtnRoute);
            if (isBtnRoute) { //길찾기 버튼일때
                openRouteSearch();
            } else {
                //검색결과 닫기
                clearSearchResult();
                $("#btn_road").attr('class', 'btn_road');
                $(".wrap_searchboxLy").css({
                    "display": "none",
                });
                $(".geo_box").css({
                    "bottom": "auto", //영역 차지 방지
                });
                $(".wrap_searchbox").css({
                    "display": "none"
                });
            }
        } else { //하나라도 꺼져 있을 경우 => 길찾기가 꺼져있을때 target
            //검색결과 닫기
            clearSearchResult();
            $(".wrap_searchboxLy").css({
                "display": "none",
            });
            $(".geo_box").css({
                "bottom": "auto", //영역 차지 방지
            });
            $(".wrap_searchbox").css({
                "display": "none"
            });
        }
    }

    var closeAddressSearchResult = function() {
        $(".wrap_searchboxLy").css({
            "display": "none",
            "position": "absolute",
            "top": "33px",
            "bottom": "0px",
            "border": "1px solid #728aa2;"
        });
        $(".wrap_searchbox").css({
            "display": "none"
        });
        $(".btn_open03").css({
            "display": ""
        });
        $("#btn_road").attr('class', 'btn_road');
        // setBtnRoad('btn_search_close');
    }

    function openRouteSearch() {
        //길찾기 시작
        $("#route_drawer").stop().animate({
            left: '10px'
        }, 250);
        $(".wrap_Gsearch").css({
            "display": "none"
        });
        $(".btn_route_drawer_arrow").css({
            "display": ""
        });
    }

    var setUIControl = function(id, state) {
        $('#' + id).prop('checked', state);
    }

    /**
     * @description UI를 끄거나 켜는 메인 함수
     * @param {string} _id toggle switch의 id
     * @param {bool} _state 설정 할 state 값(true : visible , false : hidden)
     */
    var UIControl = function(_id, _state) {
        var id = _id.split("_")[2];
        var state = _state;
        var mapOpts = {};

        console.log('kde', id + " " + state);
        var value = 'hidden';
        if (state) {
            value = 'visible';
        }
        switch (id) {
            case "search":
                toggleAddressRoute();
                break;
            case "routeSearch":
                var display = 'none';
                if (state) {
                    display = 'block';
                }
                $('#context_menu_1').css('display', display);
                $('#context_menu_2').css('display', display);
                toggleAddressRoute();
                break;
            case "centerAddress":
                toggleCenterInfo();
                break;
            case "weather":
                toggleCenterInfo();
                break;
            case "selectMap":
                $('.control-roadmap-selected').css('visibility', value);
                $('.control-roadmap-out').css('visibility', value);
                $('.control-satellite-selected').css('visibility', value);
                $('.control-satellite-out').css('visibility', value);
                $('.control-hybrid-selected').css('visibility', value);
                $('.control-hybrid-out').css('visibility', value);
                break;
            case "zoomControl":
                $('.ollehmap-zoom-control').css('visibility', value);
                break;
            case "category":
                $('#wrap_category').css('visibility', value);
                if($('.wrap_facilty').is(":visible")){
                    markerManager.clearMarkerCluster();
                    for (key in categoryMarkerList) {
                        markerManager.removeMarker(categoryMarkerList[key]);
                    }
                }
                break;
            case "measureMap":
                $('.control-distance-out').css('visibility', value);
                $('.control-distance-selected').css('visibility', value);
                $('.control-shape-out').css('visibility', value);
                $('.control-shape-selected').css('visibility', value);
                $('.control-change').css('visibility', value);
                break;
            case "scale":
                $('.ollehmap-scale-control').css('visibility', value);
                break;
            case "panToCenter":
                $('#wrap_present').css('visibility', value);
                break;
            case "centerMarker":
                markerManager.toggleCenterMarker(state);
                break;
        }
        storageManager.saveUIControl(_id, state); //split 하지 않은 id를 저장
    }

    function toggleAddressRoute() {
        var address = $('#btn_ui_search').is(':checked');
        var route = $('#btn_ui_routeSearch').is(':checked');

        if (address && route) { //둘다 on
            $('.btn_ui_setting_float').css('visibility', 'hidden');
            $('#btn_route_drawer_close').css('visibility', 'visible');
            $('.btn_search_select_route').css('visibility', 'visible');
            $('.geo_box').css('visibility', '');
            $('#btn_road').attr('class', 'btn_road');
            // setBtnRoad('btn_road');
        } else if (address == false && route == false) { //둘다 off
            $('.geo_box').css('visibility', 'hidden');
            clearSearchResult();
            closeRouteSearch();
            closeSearchInRoute();
            clearRouteSearchResult();
            clearRouteGuideResult();
            $('.btn_ui_setting_float').css('visibility', 'visible');
            console.log('kde', $('.btn_ui_setting_float'));
        } else if (address == true && route == false) { //주소검색만 on
            $('.geo_box').css('visibility', 'visible');
            $('#btn_road').attr('class', 'btn_search_close');
            $('.btn_search_select_route').css('visibility', 'hidden');
            // setBtnRoad('btn_search_close');
            closeRouteSearch();
            closeSearchInRoute();
            clearRouteSearchResult();
            clearRouteGuideResult();
            $('.btn_ui_setting_float').css('visibility', 'hidden');
        } else if (address == false && route == true) { //경로검색만 on
            openRouteSearch();
            $('.geo_box').css('visibility', 'hidden');
            clearSearchResult();
            $('#btn_route_drawer_close').css('visibility', 'hidden');
        }
    }

    function toggleCenterInfo() {
        var address = $('#btn_ui_centerAddress').is(':checked');
        var weather = $('#btn_ui_weather').is(':checked');
        console.log('kde', address + " " + weather);

        if (address && weather) { //둘다 on
            $('#center_address_wrap').css('border', '.5px solid');
            $('#center_address_wrap').css('background', '#FFFFFF');
            $('#center_address').css('border', '0px');
            $('#center_address').css('background', 'transparent');
            $('#center_weather').css('border', '0px');
            $('#center_weather').css('background', 'transparent');
        } else { //둘중 하나 off
            $('#center_address_wrap').css('border', '0px');
            $('#center_address_wrap').css('background', 'transparent');
            $('#center_address').css('border', '.5px solid');
            $('#center_address').css('border-radius', '3px');
            $('#center_address').css('background', '#FFFFFF');
            $('#center_weather').css('border', '.5px solid');
            $('#center_weather').css('border-radius', '3px');
            $('#center_weather').css('background', '#FFFFFF');
        }

        if (address) {
            $('#center_address').css('visibility', '');
        } else {
            $('#center_address').css('visibility', 'hidden');
        }

        if (weather) {
            $('#center_weather').css('visibility', '');
        } else {
            $('#center_weather').css('visibility', 'hidden');
            if ($(".wrap_weather").is(":visible")) {
                $(".wrap_weather").hide();
                $("#btn_toggle_weather").text("▼");
            }
        }
    }

    var toggleSearchOptBtn = function(sortBy) {
        switch (sortBy) {
            case "RELEVANCE":
                $('#btn_distance').attr('class', 'clickable');
                $('#btn_relevance').attr('class', 'on btn_disable');
                break;
            case "DISTANCE":
                $('#btn_distance').attr('class', 'on btn_disable');
                $('#btn_relevance').attr('class', 'clickable');
                break;
        }
    }

    function toggleWeather() {
        if ($(".wrap_weather").is(":visible")) {
            $(".wrap_weather").hide();
            $("#btn_toggle_weather").text("▼");
        } else {
            $(".wrap_weather").show();
            $("#btn_toggle_weather").text("▲");
        }
    }

    var toggleRouteSearchInput = function(state) {
        var value = 'hidden';
        if (state) {
            value = 'visible';
        }
        $('.btn_remove_via').css('visibility', value);
        if (state) {
            $('.wrap_route_Gsearch').css('background', '#FFFFFF');
            $('.input_act').css('background', '#FFFFFF');
        } else {
            $('.wrap_route_Gsearch').css('background', '#EEE');
            $('.input_act').css('background', '#EEE');
        }
    }

    function closeRouteSearch() {
        $("#route_drawer").stop().animate({
            left: '-400px'
        }, 250);
        $(".wrap_Gsearch").css({
            "display": ""
        });
    }

    function openSearchInRoute() {
        //길찾기 시작
        $("#search_in_route").stop().animate({
            top: '10px'
        }, 250);
    }

    function closeSearchInRoute() {
        //길찾기 시작
        $("#search_in_route").stop().animate({
            top: '-50px'
        }, 250);
        //검색어 초기화
        $('#inp_search_in_route').val('');
        //버튼 초기화
        $('#btn_search_in_route').attr('class', 'btn_search');
    }

    /**
     * @description 날씨 api의 호출 결과를 set하는 함수
     * @param {response} response api의 json response
     */
    var setWeather = function(response) {
        var weather = response.weather.minutely[0];
        //현재 기온
        var tc = parseFloat(weather.temperature.tc);
        $('#txt_temp').text(tc.toFixed(1) + "˚C");
        $('#txt_temp_2').text(tc.toFixed(1) + "˚C");

        //최저, 최고 기온
        var tmax = parseFloat(weather.temperature.tmax);
        var tmin = parseFloat(weather.temperature.tmin);
        $('#tmp_min_max').text(tmin.toFixed(0) + "/" + tmax.toFixed(0));

        //풍속
        var wspd = parseFloat(weather.wind.wspd);
        $('#wspd').text(wspd.toFixed(1) + "m/s");

        //강수량
        var precipitation = parseFloat(weather.precipitation.sinceOntime);
        $('#precipitation').text(precipitation + "mm");

        //날씨 상태 icon set
        var sky = weather.sky.name;
        var src = '';
        switch (sky) {
            case "맑음":
                src = './images_ex/ic_weather_sunny.png';
                break;
            case "구름조금":
                src = './images_ex/ic_weather_sun_cloud.png';
                break;
            case "구름많음":
                src = './images_ex/ic_weather_cloud.png';
                break;
            case "구름많고 비":
                src = './images_ex/ic_weather_rain.png';
                break;
            case "구름많고 눈":
                src = './images_ex/ic_weather_cloud_snow.png';
                break;
            case "구름많고 비 또는 눈":
                src = './images_ex/ic_weather_cloud_snow.png';
                break;
            case "흐림":
                src = './images_ex/ic_weather_cloud.png';
                break;
            case "흐리고 비":
                src = './images_ex/ic_weather_rain.png';
                break;
            case "흐리고 눈":
                src = './images_ex/ic_weather_cloud_snow.png';
                break;
            case "흐리고 비 또는 눈":
                src = './images_ex/ic_weather_cloud_snow.png';
                break;
            case "흐리고 낙뢰":
                src = './images_ex/ic_weather_thunder.png';
                break;
            case "뇌우/비":
                src = './images_ex/ic_weather_rain_thunder.png';
                break;
            case "뇌우/눈":
                src = './images_ex/ic_weather_rain_thunder.png';
                break;
            case "뇌우/비 또는 눈":
                src = './images_ex/ic_weather_rain_thunder.png';
                break;
        }
        $('#weather_img').attr("src", src);
    }

    /**
     * @description autocomplete결과를 set한다.
     * @see searchManager.autoComplete()
     */
    var setAutocompleteResult = function(response) {
        console.log(LOG_HEAD + "setAutocompleteResult\n", $('#list_suggest_address').children('a'));
        var listAddress = $('#list_suggest_address').children('a');
        var listRouteSrt = $('#list_suggest_route_start').children('a');
        var listRouteV1 = $('#list_suggest_route_via_1').children('a');
        var listRouteV2 = $('#list_suggest_route_via_2').children('a');
        var listRouteV3 = $('#list_suggest_route_via_3').children('a');
        var listRouteEnd = $('#list_suggest_route_end').children('a');

        for (var i = 0; i < listAddress.length; i++) {
            //var index = $(this).index();
            var listAddressItem = listAddress[i];
            var listRouteSrtItem = listRouteSrt[i];
            var listRouteV1Item = listRouteV1[i];
            var listRouteV2Item = listRouteV2[i];
            var listRouteV3Item = listRouteV3[i];
            var listRouteEndItem = listRouteEnd[i];

            var suggest = response.suggests[i];

            $(listAddressItem).text(suggest.terms);
            $(listRouteSrtItem).text(suggest.terms);
            $(listRouteV1Item).text(suggest.terms);
            $(listRouteV2Item).text(suggest.terms);
            $(listRouteV3Item).text(suggest.terms);
            $(listRouteEndItem).text(suggest.terms);
        }

        autoCompleteData = response.suggests;
    }

    /**
     * @description autocomplete이 클릭 되었을 때 이벤트를 binding한다.
     */
    var bindAddressAutocomplete = function() {
        var listId = ['address', 'route_start', 'route_via_1', 'route_via_2', 'route_via_3', 'route_end'];

        for (var i = 0; i < listId.length; i++) {
            $("#list_suggest_" + listId[i]).on("click", "a", function() {
                //클릭된 item index를 가져온다
                var index = $(this).index();
                var selectedItem = autoCompleteData[index];
                // console.log(LOG_HEAD+"bindAddressAutocomplete\n",$(this).parent().prev().prevAll("input").attr("class"));
                var classAllName = $(this).parent().prev().prevAll("input").attr("class") //input_act start으로 옴
                var type = classAllName.split(' ')[1];
                console.log(LOG_HEAD + "bindAddressAutocomplete\n", type);
                //index의 데이터를 search 창에 입력한뒤 검색한다.
                $(this).parent().prev().prevAll("input").val(selectedItem.terms);

                onClickSearchBtn(type);
            });
        }
    }

    /**
     * @description 검색기록이 클릭 되었을 때 이벤트를 binding한다.
     */
    var bindHistoryItems = function() {
        var listId = ['address', 'route_start', 'route_via_1', 'route_via_2', 'route_via_3', 'route_end'];

        for (var i = 0; i < listId.length; i++) {
            $("#list_history_" + listId[i]).on("click", "a", function() {
                //클릭된 item index를 가져온다
                var index = $(this).index();
                var history = storageManager.getHistory();
                var selectedItem = history[history.length - 1 - index];
                if (selectedItem == null || selectedItem == '') {
                    return false;
                }
                // console.log(LOG_HEAD+"bindAddressAutocomplete\n",$(this).parent().prev().prevAll("input").attr("class"));
                var classAllName = $(this).parent().prev().prevAll("input").attr("class") //input_act start으로 옴
                var type = classAllName.split(' ')[1];
                console.log(LOG_HEAD + "bindHistoryItems\n", $(this).closest("input"));
                //index의 데이터를 search 창에 입력한뒤 검색한다.
                $(this).parent().prev().prevAll("input").val(selectedItem);

                onClickSearchBtn(type);
            });
        }
    }

    var bindAddressSearchResultItems = function(domId, response) {
        var searchResultItems = $("#" + domId).children('li');
        console.log(LOG_HEAD, searchResultItems);
        for (var i = 0; i < searchResultItems.length; i++) {
            $(searchResultItems[i]).hover(
                function() { //hover in
                    $($(this).attr('class', 'on'));
                    var index = $(this).index();
                    var geographicInformation = response.residentialAddress[index].parcelAddress[0].geographicInformation;
                    var shape = geographicInformation.shape;
                    var path = shape.coordinates[0][0];
                    console.log(LOG_HEAD + "coordinates", path);
                    vectorManager.drawMultipolygon(path);
                },
                function() { //hover out
                    $($(this).attr('class', ''));
                    vectorManager.removeAllMultipolygons();
                });
        }
    }

    var bindPlaceSearchResultItems = function(domId, response, type) {
        var searchResultItems = $("#" + domId).children('li');
        for (var i = 0; i < searchResultItems.length; i++) {
            $(searchResultItems[i]).hover(
                function() { //hover in
                    $($(this).attr('class', 'on'));
                    var index = $(this).index();
                    var poi = response.pois[index];
                    var position = new olleh.maps.LatLng(poi.point.lat, poi.point.lng);
                    console.log(LOG_HEAD + "placeMarkerList\n", placeMarkerList[poi.id]);
                    markerManager.setMarkerSize(placeMarkerList[poi.id], new olleh.maps.Size(50, 50)); //make marker big
                    var caption = {
                        title: poi.name,
                        color: 'black',
                        backgroundColor: 'white'
                    }
                    markerManager.setMarkerZIndex(placeMarkerList[poi.id], 900000);
                    //markerManager.setMarkerCaption(placeMarkerList[poi.id], caption, true);
                    markerManager.setTooltip(placeMarkerList[poi.id], poi.name + ' ' + (poi.subName || '') + (poi.branch && '(' + poi.branch + ')'));

                },
                function() { //hover out
                    $($(this).attr('class', ''));
                    var index = $(this).index();
                    var poi = response.pois[index];
                    markerManager.setMarkerSize(placeMarkerList[poi.id], new olleh.maps.Size(40, 40)); //make marker small
                    //markerManager.setMarkerCaption(placeMarkerList[poi.id], null, false);
                    markerManager.setMarkerZIndex(placeMarkerList[poi.id], null);
                    markerManager.toggleTooltip(placeMarkerList[poi.id], false);
                });
            $(searchResultItems[i]).click(
                function() {
                    var index = $(this).index();
                    var poi = response.pois[index];
                    var position = new olleh.maps.LatLng(poi.point.lat, poi.point.lng);

                    var tmpl =
                        '<div style="font-family: dotum, arial, sans-serif;font-size: 13px; font-weight: bold;margin-bottom: 5px;">#{title}</div>' +
                        '<table style="border-spacing: 2px; border: 0px"><tbody>' +
                        '<tr><td style="color:#767676;padding-right:12px">분류</td>' +
                        '<td><span>#{category}</span></td></tr>' +
                        '<tr><td style="color:#767676;padding-right:12px">신주소</td>' +
                        '<td><span>#{roadAddr}</span></td></tr>' +
                        '<tr><td style="color:#767676;padding-right:12px">지번</td>' +
                        '<td style=""><span>#{addr}</span></td></tr>' +
                        '<tr><td style="color:#767676;padding-right:12px">전화번호</td>' +
                        '<td><span>#{tel}</span></td>' +
                        '</tr></tbody></table>';

                    var data = {
                        title: poi.name + ' ' + (poi.subName || '') + (poi.branch && '(' + poi.branch + ')'),
                        category: poi.category.masterName + " > " + poi.category.middleName,
                        roadAddr: poi.address.siDo + " " + poi.address.siGunGu + (poi.address.street && " " + poi.address.street || "") + (poi.address.streetNumber && " " + poi.address.streetNumber || ""),
                        addr: (poi.address.eupMyeon && " " + poi.address.eupMyeon || "") + (poi.address.eupMyeonDong && " " + poi.address.eupMyeonDong || "") + (poi.address.ri && " " + poi.address.ri || "") + (poi.address.houseNumber && ' ' + poi.address.houseNumber || ''),
                        tel: (poi.phones && poi.phones.representation || ""),
                    };

                    var content = olleh.maps.util.applyTemplate(tmpl, data);
                    markerManager.setInfoWindow(placeMarkerList[poi.id], content);
                    console.log("kde", poi);
                });
            var startBtn = $(searchResultItems[i]).find('.btn_search_select_route')[0];
            var endBtn = $(searchResultItems[i]).find('.btn_search_select_route')[1];
            $(startBtn).click(
                function() {
                    var index = $(this).closest('li').index();
                    setRouteSearch(response.pois[index], "start");
                });
            $(endBtn).click(
                function() {
                    var index = $(this).closest('li').index();
                    setRouteSearch(response.pois[index], "end");
                });
        }
    }

    function bindRoutePlaceSearchResultItems(domId, response, type) {
        if (type != "address") {
            // if (tempMarker == null) {
            var position = new olleh.maps.LatLng(37, 20);
            //     var markerName = markerManager.setImageMarker("./images_ex/marker_red.png", position);
            //     tempMarker = {
            //         markerName: markerName,
            //         position: position
            //     };
            // }
            markerManager.setTempMarker(position);
        }
        markerManager.toggleTempMarker(false);

        var searchResultItems = $("#" + domId).children('li');
        console.log(LOG_HEAD, searchResultItems);
        for (var i = 0; i < searchResultItems.length; i++) {
            $(searchResultItems[i]).hover(
                function() { //hover in
                    var index = $(this).index();
                    var poi = response.pois[index];
                    var position = new olleh.maps.LatLng(poi.point.lat, poi.point.lng);
                    console.log(LOG_HEAD, type);
                    $($(this).attr('class', 'on'));

                    //markerManager.setPositionMarker(tempMarker.markerName, position);
                    //markerManager.toggleMarker(tempMarker.markerName, true);
                    //markerManager.moveToMarker(tempMarker.markerName);
                    markerManager.setTempMarker(position);
                    markerManager.toggleTempMarker(true);
                    markerManager.panToTempMarker();
                },
                function() { //hover out
                    $($(this).attr('class', ''));
                    var index = $(this).index();
                    var poi = response.pois[index];

                    //markerManager.toggleMarker(tempMarker.markerName, false);
                    markerManager.toggleTempMarker(false);
                });
            $(searchResultItems[i]).click(
                function() {
                    var index = $(this).index();
                    var poi = response.pois[index];
                    var position = new olleh.maps.LatLng(poi.point.lat, poi.point.lng);
                    if (type == "start") {
                        if (startMarker.markerName != '') {
                            markerManager.removeMarker(startMarker.markerName);
                        }
                        var markerName = markerManager.setImageMarker("./images_ex/ic_flag_red.png", position);
                        startMarker = {
                            markerName: markerName,
                            markerPosition: position
                        };
                        console.log(LOG_HEAD + "start\n", poi.name);
                        $('#inp_route_start_search').val(poi.name + " " + (poi.subName || "") + (poi.branch && "(" + poi.branch + ")"));
                    } else if (type == "via_1") {
                        if (via1Marker.markerName != '') {
                            markerManager.removeMarker(endMarker.markerName);
                        }
                        var markerName = markerManager.setImageMarker("./images_ex/ic_flag_blue.png", position);
                        via1Marker = {
                            markerName: markerName,
                            markerPosition: position
                        };
                        $('#inp_route_via_1_search').val(poi.name + " " + (poi.subName || "") + (poi.branch && "(" + poi.branch + ")"));
                    } else if (type == "via_2") {
                        if (via2Marker.markerName != '') {
                            markerManager.removeMarker(endMarker.markerName);
                        }
                        var markerName = markerManager.setImageMarker("./images_ex/ic_flag_blue.png", position);
                        via2Marker = {
                            markerName: markerName,
                            markerPosition: position
                        };
                        $('#inp_route_via_2_search').val(poi.name + " " + (poi.subName || "") + (poi.branch && "(" + poi.branch + ")"));
                    } else if (type == "via_3") {
                        if (via3Marker.markerName != '') {
                            markerManager.removeMarker(endMarker.markerName);
                        }
                        var markerName = markerManager.setImageMarker("./images_ex/ic_flag_blue.png", position);
                        via3Marker = {
                            markerName: markerName,
                            markerPosition: position
                        };
                        $('#inp_route_via_3_search').val(poi.name + " " + (poi.subName || "") + (poi.branch && "(" + poi.branch + ")"));
                    } else if (type == "end") {
                        if (endMarker.markerName != '') {
                            markerManager.removeMarker(endMarker.markerName);
                        }
                        var markerName = markerManager.setImageMarker("./images_ex/ic_flag_green.png", position);
                        endMarker = {
                            markerName: markerName,
                            markerPosition: position
                        };
                        $('#inp_route_end_search').val(poi.name + " " + (poi.subName || "") + (poi.branch && "(" + poi.branch + ")"));
                    }
                    checkRoute();
                });
        }
    }

    var bindRouteGuideItems = function(domId, response) {
        var searchResultItems = $("#" + domId).children('li');
        console.log(LOG_HEAD, searchResultItems);
        for (var i = 0; i < searchResultItems.length; i++) {
            $(searchResultItems[i]).hover(
                function() { //hover in
                    $($(this).attr('class', 'on'));
                    var index = $(this).index();
                    var routeMarkersName = markerManager.getRouteMarkersName();
                    var name = routeMarkersName["route_" + index];
                    markerManager.toggleMarker(name, true);
                },
                function() { //hover out
                    $($(this).attr('class', ''));
                    var index = $(this).index();
                    var routeMarkersName = markerManager.getRouteMarkersName();
                    var name = routeMarkersName["route_" + index];
                    markerManager.toggleMarker(name, false);
                });
        }
    }

    var openHistoryList = function(domInput) {
        //주소 검색창 자동완성 기능
        console.log(LOG_HEAD + "openSuggestList\n", $(domInput).attr('class'));
        var className = $(domInput).attr('class');
        $(".wrap_history").css({
            "display": "none"
        });
        $(".wrap_route_history").css({
            "display": "none"
        });
        switch (className) {
            case 'input_act address':
                $("#list_history_address").css({
                    "display": "block"
                });
                break;
            case 'input_act start':
                $("#list_history_route_start").css({
                    "display": "block"
                });
                break;
            case 'input_act end':
                $("#list_history_route_end").css({
                    "display": "block"
                });
                break;
            case 'input_act via_1':
                $("#list_history_route_via_1").css({
                    "display": "block"
                });
                break;
            case 'input_act via_2':
                $("#list_history_route_via_2").css({
                    "display": "block"
                });
                break;
            case 'input_act via_3':
                $("#list_history_route_via_3").css({
                    "display": "block"
                });
                break;
            case 'input_act end':
                $("#list_history_route_via_end").css({
                    "display": "block"
                });
                break;
        }
    }

    var setHistoryList = function() {
        var historyArr = storageManager.getHistory();
        console.log(LOG_HEAD + "setHistoryList", historyArr);

        var listAddress = $('#list_history_address').children('a');
        var listRouteSrt = $('#list_history_route_start').children('a');
        var listRouteV1 = $('#list_history_route_via_1').children('a');
        var listRouteV2 = $('#list_history_route_via_2').children('a');
        var listRouteV3 = $('#list_history_route_via_3').children('a');
        var listRouteEnd = $('#list_history_route_end').children('a');

        for (var i = 0; i < listAddress.length; i++) {
            //var index = $(this).index();
            var listAddressItem = listAddress[i];
            var listRouteSrtItem = listRouteSrt[i];
            var listRouteV1Item = listRouteV1[i];
            var listRouteV2Item = listRouteV2[i];
            var listRouteV3Item = listRouteV3[i];
            var listRouteEndItem = listRouteEnd[i];

            var index = historyArr.length - 1 - i;
            $(listAddressItem).text(historyArr[index] || '');
            $(listRouteSrtItem).text(historyArr[index] || '');
            $(listRouteV1Item).text(historyArr[index] || '');
            $(listRouteV2Item).text(historyArr[index] || '');
            $(listRouteV3Item).text(historyArr[index] || '');
            $(listRouteEndItem).text(historyArr[index] || '');
        }
    }

    var openSuggestList = function(domInput) {
        //주소 검색창 자동완성 기능
        console.log(LOG_HEAD + "openSuggestList\n", $(domInput).attr('class'));
        var className = $(domInput).attr('class');
        $(".wrap_history").css({
            "display": "none"
        });
        $(".wrap_route_history").css({
            "display": "none"
        });
        switch (className) {
            case 'input_act address':
                $("#list_suggest_address").css({
                    "display": "block"
                });
                break;
            case 'input_act start':
                $("#list_suggest_route_start").css({
                    "display": "block"
                });
                break;
            case 'input_act end':
                $("#list_suggest_route_end").css({
                    "display": "block"
                });
                break;
            case 'input_act via_1':
                $("#list_suggest_route_via_1").css({
                    "display": "block"
                });
                break;
            case 'input_act via_2':
                $("#list_suggest_route_via_2").css({
                    "display": "block"
                });
                break;
            case 'input_act via_3':
                $("#list_suggest_route_via_3").css({
                    "display": "block"
                });
                break;
            case 'input_act end':
                $("#list_suggest_route_via_end").css({
                    "display": "block"
                });
                break;
        }
    }

    //검색 로딩 화면 toggle
    var toggleAjaxLayer = function(state) {
        if (state == true) {
            $('#search_result_container').css("display", "none");
            $('#ajax_loading_layer').css("display", '');
            $('#route_search_result_container').css("display", "none");
            $('#route_ajax_loading_layer').css("display", '');
        } else {
            $('#search_result_container').css("display", "");
            $('#ajax_loading_layer').css("display", 'none');
            $('#route_search_result_container').css("display", "");
            $('#route_ajax_loading_layer').css("display", 'none');
        }
    }

    /**
     * @description 검색결과의 아이템을 추가해준다.
     * @param {string} domId 아이템이 추가될 부모 dom id
     * @param {int} index 현재 아이템의 index
     * @param {json} parcelAddress api response의 parcelAddress
     */
    function addAddressSearchResultItem(domId, index, parcelAddress) {
        console.log(LOG_HEAD, parcelAddress);
        var $item = $(
            "<li class=''><a class='clickable'><span class='pos_num'>" + index +
            "</span><strong>" + parcelAddress[0].fullAddress + "</strong></a></li>"
        );

        $("#" + domId).append($item);
    }

    function addPlaceSearchResultItem(domId, index, poi, type) {

        var visibility = '';

        var address = $('#btn_ui_search').is(':checked');
        var route = $('#btn_ui_routeSearch').is(':checked');
        if (address == true && route == false) { //주소검색만 on
            $('.btn_search_select_route').css('visibility', 'hidden');
            visibility = "style='visibility:hidden'";
        }

        var $item = $(
            "<li class=''><a><span class='pos_num'>" + index + "</span><table width='95%'><tr><td colspan='2' style='width:75%'>" +
            "<strong style='font-size:12px'>" + poi.name + " " + (poi.subName || "") + (poi.branch && "(" + poi.branch + ")") + "</strong></td><td rowspan='4' style='width:25%'><button class='btn_search_select_route'"+ visibility +">출발</button><button class='btn_search_select_route'"+ visibility +">도착</button></td></tr><tr><td style='border-right:.3px solid;border-color:LightGray;width:40%'>" +
            "<font style='color:DodgerBlue;text-stlye:'>☎ " + (poi.phones && poi.phones.representation || "") + "</font></td><td style='width:35%''>" +
            "<span class='txt_branch'>" + poi.category.masterName +
            "</span></td></tr><tr>" +
            "<td colspan='2'>" +
            poi.address.siDo + " " +
            poi.address.siGunGu +
            (poi.address.street && " " + poi.address.street || "") +
            (poi.address.streetNumber && " " + poi.address.streetNumber || "") +
            "</td></tr><tr><td colspan='2'><span class='ico_anumber'>지번</span><span>" +
            (poi.address.eupMyeon && " " + poi.address.eupMyeon || "") +
            (poi.address.eupMyeonDong && " " + poi.address.eupMyeonDong || "") +
            (poi.address.ri && " " + poi.address.ri || "") +
            (poi.address.houseNumber && " " + poi.address.houseNumber || "") +
            "</span></td></tr></table></a></li>");

        var $routeItem = $(
            "<li class=''><a class='clickable'><span class='pos_num'>" + index + "</span><table width='95%'><tr><td colspan='2' style='width:75%'>" +
            "<strong style='font-size:12px'>" + poi.name + " " + (poi.subName || "") + (poi.branch && "(" + poi.branch + ")") + "</strong></td></tr><tr><td style='border-right:.3px solid;border-color:LightGray;width:40%'>" +
            "<font style='color:DodgerBlue;text-stlye:'>☎ " + (poi.phones && poi.phones.representation || "") + "</font></td><td style='width:35%''>" +
            "<span class='txt_branch'>" + poi.category.masterName +
            "</span></td></tr><tr>" +
            "<td colspan='2'>" +
            poi.address.siDo + " " +
            poi.address.siGunGu +
            (poi.address.street && " " + poi.address.street || "") +
            (poi.address.streetNumber && " " + poi.address.streetNumber || "") +
            "</td></tr><tr><td colspan='2'><span class='ico_anumber'>지번</span><span>" +
            (poi.address.eupMyeon && " " + poi.address.eupMyeon || "") +
            (poi.address.eupMyeonDong && " " + poi.address.eupMyeonDong || "") +
            (poi.address.ri && " " + poi.address.ri || "") +
            (poi.address.houseNumber && " " + poi.address.houseNumber || "") +
            "</span></td></tr></table></a></li>");
        if (type == "place") {
            $("#" + domId).append($item);
        } else {
            $("#" + domId).append($routeItem);
        }
    }

    var addRouteSearchResultItem = function(domId, index, rgInfo) {
        console.log(LOG_HEAD, rgInfo);
        var node = '';
        if (rgInfo.getNodeName() != '') {
            node += rgInfo.getNodeName();
            node += "<br /><br />";
        }

        var guide = '';
        if (rgInfo.getDirName() != '') {
            guide += rgInfo.getDirName() + " 방면으로 ";
        }
        if (rgInfo.getType() != '') {
            guide += rgInfo.getType();
            if (rgInfo.getType() != '출발지' && rgInfo.getType() != '경유지' && rgInfo.getType() != '목적지') {
                guide += " 후 ";
            }
        }
        if (rgInfo.getNextDist() != '') {
            if (rgInfo.getType() != '목적지') {
                guide += rgInfo.getNextDist() + " m 이동 ";
            }
        }

        var item =
            "<li><a><span class='pos_num'>" + index + "</span><table width='95%'><tr><td width='80%'>" +
            "<strong style='font-size:12px'>" + node + "</strong>" +
            guide +
            // "</td><td style='width:20%;text-align:center'><img src='images/icon_road_5_2.png' alt='앞으로' /></td></tr></table></a></li>"
            "</td><td style='width:20%;text-align:center'><img src='images_ex/ic_" + rgInfo.getGuideImg() + ".png'></td></tr></table></a></li>"

        $("#" + domId).append(item);
    }

    var removeSearchResultItems = function(domId) {
        console.log(LOG_HEAD + "removeSearchResultItems\n", domId);
        $("#" + domId).empty();
    }

    var changeSearchResultTab = function(index) {
        switch (index) {
            case 0:
                searchAll(searchManager.getSearchOpt());
                $('#search_tab_all').attr("class", "on");
                $('#search_tab_place').attr("class", "");
                $('#search_tab_address').attr("class", "");
                $('#address_tab_all').css({
                    'display': ''
                });
                $('#address_tab_place').css({
                    'display': 'none'
                });
                $('#address_tab_address').css({
                    'display': 'none'
                });
                break;
            case 1:
                searchPlace(searchManager.getPlaceSearchOpt());
                $('#search_tab_all').attr('class', '');
                $('#search_tab_place').attr('class', 'on');
                $('#search_tab_address').attr('class', '');
                $('#address_tab_all').css({
                    'display': 'none'
                });
                $('#address_tab_place').css({
                    'display': ''
                });
                $('#address_tab_address').css({
                    'display': 'none'
                });
                break;
            case 2:
                searchAddress(searchManager.getSearchOpt());
                $('#search_tab_all').attr("class", "");
                $('#search_tab_place').attr("class", "");
                $('#search_tab_address').attr("class", "on");
                $('#address_tab_all').css({
                    'display': 'none'
                });
                $('#address_tab_place').css({
                    'display': 'none'
                });
                $('#address_tab_address').css({
                    'display': ''
                });
                break;
        }
    }

    function init(_map) {
        console.log(LOG_HEAD, "init");
        map = _map;

        return {
            setUIControl: setUIControl,
            UIControl: UIControl,
            onClickBtnRoad: onClickBtnRoad,
            setWeather: setWeather,
            openAddressSearchResult: openAddressSearchResult,
            closeAddressSearchResult: closeAddressSearchResult,
            openSearchInRoute: openSearchInRoute,
            closeSearchInRoute: closeSearchInRoute,
            openRouteSearch: openRouteSearch,
            closeRouteSearch: closeRouteSearch,
            toggleSearchOptBtn: toggleSearchOptBtn,
            toggleWeather: toggleWeather,
            toggleRouteSearchInput: toggleRouteSearchInput,
            bindAddressAutocomplete: bindAddressAutocomplete,
            bindHistoryItems: bindHistoryItems,
            openHistoryList: openHistoryList,
            setHistoryList: setHistoryList,
            openSuggestList: openSuggestList,
            toggleAjaxLayer: toggleAjaxLayer,
            bindAddressSearchResultItems: bindAddressSearchResultItems,
            bindRoutePlaceSearchResultItems: bindRoutePlaceSearchResultItems,
            bindPlaceSearchResultItems: bindPlaceSearchResultItems,
            bindRouteGuideItems: bindRouteGuideItems,
            setAutocompleteResult: setAutocompleteResult,
            removeSearchResultItems: removeSearchResultItems,
            addAddressSearchResultItem: addAddressSearchResultItem,
            addPlaceSearchResultItem: addPlaceSearchResultItem,
            addRouteSearchResultItem: addRouteSearchResultItem,
            changeSearchResultTab: changeSearchResultTab
        }
    }

    return {
        getInstance: function(map) {
            if (instance === undefined) {
                instance = init(map);
            }
            return instance;
        }
    }

})();
