/**
 * @author Dongeun Kim
 * @file search.js
 * @module SearchManager
 * @description POI 검색을 담당
 */
var SearchManager = (function() {
    var instance;

    /* API 설정 */
    var API_URL = "https://gis.kt.com/search/v1.0";
    var API_KEY = "BEARER df21e533e679ef05f19ab3e318085f629da782757fdc5c65d31bb6815687f18a61711750";

    /* 검색 시 api 호출 options */
    var searchOpt = null;
    var placeSearchOpt = null;

    var getSearchOpt = function() {
        return searchOpt;
    }
    var getPlaceSearchOpt = function() {
        return placeSearchOpt;
    }
    var setSearchOpt = function(_searchOpt) {
        searchOpt = _searchOpt;
    }
    var setPlaceSearchOpt = function(_placeSearchOpt) {
        placeSearchOpt = _placeSearchOpt;
    }

    /**
     * @description 검색어 입력시 자동완성 기능
     * @param {string} searchString 입력된 검색어
     * @param {function} callbackFunc 결과값을 callback할 함수
     */
    var autoComplete = function(searchString, callbackFunc) {
        var params = {
            terms: searchString
        };
        callAPI("autocomplete", 'get', params, callbackFunc);
    }

    /**
     * @description POI 검색 함수
     * @param {string} searchString 입력된 검색어
     * @param {array} callbackFunc 결과값을 callback할 함수 배열
     * @param {object} searchOpt 검색 옵션을 가지고 있는 object
     */
    var searchPOI = function(searchString, callbackFuncs, searchOpt) {
        //현재 페이지 = (currentBtnIndex + 1) + (pageIndex * PAGE_BTN_COUNT);

        var params = {
            terms: searchString,
            start: ((searchOpt.currentBtnIndex) + (searchOpt.pageIndex * 5)) * 10, // 5랑 10을 상수로 설정해도 될듯
            searchType: searchOpt.searchType //address, start, via_1 ...
        };

        if (searchOpt.sortBy == "DISTANCE") {
            params['sortBy'] = 'DISTANCE';
            params['point.lat'] = searchOpt.lat;
            params['point.lng'] = searchOpt.lng;
        }
        console.log(LOG_HEAD, params.start);
        callAPI("poi", 'get', params, callbackFuncs);
    }

    /**
     * @description 카테고리 검색 함수
     * @param {array} callbackFuncs 결과값을 callback할 함수 배열
     * @param {object} searchOpt 검색 옵션을 가지고 있는 object(카테고리 옵션이 들어있다.)
     */
    var searchCategoryPlace = function(callbackFuncs, searchOpt) {
        //현재 페이지 = (currentBtnIndex + 1) + (pageIndex * PAGE_BTN_COUNT);

        var params = {
            sortBy: "DISTANCE",
            'point.lat': searchOpt.lat,
            'point.lng': searchOpt.lng,
            'theme.code': searchOpt.code,
            terms: '',
            start: 0, // 5랑 10을 상수로 설정해도 될듯
            numberOfResults: 50,
        };

        callAPI("poi", 'get', params, callbackFuncs);
    }

    /**
     * @description 경로 내 검색 함수
     * @param {array} callbackFuncs 결과값을 callback할 함수 배열
     * @param {object} searchOpt 검색 옵션을 가지고 있는 object(경로 옵션이 들어있다.)
     */
    var searchInRoute = function(callbackFuncs, searchOpt) {
        var params = {
            terms: searchOpt.keyword,
            numberOfResults: 100,
        };

        var route = {
            points: searchOpt.route,
            distance: searchOpt.distance,
        };

        params['route'] = route;

        callAPI("poi", 'post', JSON.stringify(params), callbackFuncs);
    }

    /**
     * @description 특정 위치의 행정 주소를 반환하는 함수
     * @param {object} point 위도 경도를 가지고 있는 objcet { x : #, y : #}
     * @param {array} callbackFuncs 결과값을 callback할 함수 배열
     */
    var reverseGeocode = function(point, callbackFuncs) {
        var params = {
            "point.lat": point.y, //37.xxx
            "point.lng": point.x, //126.xx
        };
        callAPI("geocode", 'get', params, callbackFuncs);
    }

    function callAPI(type, callType, params, callbackFuncs) {
        var url = API_URL;
        var headers = {
            'Authorization': API_KEY,
            'Content-Type': 'Application/json',
            'Accept': 'Application/json',
            'Accept-Language': 'ko-KR'
        };

        switch (type) {
            case 'autocomplete':
                url += "/utilities/autocomplete";
                break;
            case 'poi':
                url += "/pois";
                break;
            case 'geocode':
                url += "/utilities/geocode";
            default:
                break;
        }

        $.ajax({
            type: callType,
            url: url,
            headers: headers,
            dataType: 'json',
            data: params,
            success: function(response) {
                console.log(LOG_HEAD + "callAPI\n", response);
                if (!Array.isArray(callbackFuncs)) {
                    callbackFuncs(response, params);
                } else {
                    for (var i = 0; i < callbackFuncs.length; i++) {
                        callbackFuncs[i](response, params);
                    }
                }
            },
            error: function(e) {
                console.log(e.responseText);
                alert("서버와의 연결에 실패하였습니다.");
            }
        });
    }

    /**
     *@module Pagination
     *@description 검색 결과 pagination을 위한 module
     */
    var Pagination = (function() {
        var instance;

        var pagination = {};
        var currentBtnIndex = 0; //현재 눌려진 버튼의 index
        var pageIndex = 0; //현재 보여지고있는 버튼의 페이지
        var PAGE_BTN_COUNT = 5; //한번에 보여지는 버튼의 개수
        var currentBtnIndexLimit, pageIndexLimit;
        /*
         *현재 페이지 = (currentBtnIndex + 1) + (pageIndex * PAGE_BTN_COUNT);
         *다음 페이지 넘기기 : pageIndex++; & currentBtnIndex = 0;
         */
        var paginationId = 0;

        var domObj;
        var searchFunc;
        var searchOpt;
        var btn_prev = {};
        var btn_next = {};
        var btn_page_nums = {};
        var numberOfPois = 0;

        var getDomObj = function() {
            return domObj;
        }

        var setDomObj = function(_domObj) {
            domObj = _domObj;
        }

        var setSearchFunc = function(_searchFunc) {
            searchFunc = _searchFunc;
        }

        var setSearchOpt = function(_searchOpt) {
            searchOpt = _searchOpt;
        }

        var setNumberOfPois = function(_numberOfPois) {
            numberOfPois = _numberOfPois;
            setPaginationUI();
        }

        var setPaginationUI = function() {

            currentBtnIndexLimit = parseInt(numberOfPois / 10) + 1; //보여질 수 있는 버튼의 총 갯수(not page)
            console.log(LOG_HEAD, "currentBtnIndexLimit : " + currentBtnIndexLimit);
            pageIndexLimit = Math.ceil(currentBtnIndexLimit / PAGE_BTN_COUNT) - 1;
            //all reset first
            $(domObj).css("display", "");
            $(btn_prev).css("visibility", "hidden");
            $(btn_next).css("visibility", "");
            for (var i = 0; i < btn_page_nums.length; i++) {
                $(btn_page_nums[i]).css("visibility", "");
            }
            console.log(LOG_HEAD, "pageIndex : " + pageIndex);
            if (pageIndex == 0) {
                $(btn_prev).css("visibility", "hidden");
            } else {
                $(btn_prev).css("visibility", "");
            }
            console.log(LOG_HEAD, "pageIndexLimit : " + pageIndexLimit);
            if (pageIndex == pageIndexLimit) { //마지막 페이지
                $(btn_next).css("visibility", "hidden");
                if ((currentBtnIndexLimit % PAGE_BTN_COUNT) != 0) {
                    for (var i = (currentBtnIndexLimit % PAGE_BTN_COUNT); i < btn_page_nums.length; i++) {
                        $(btn_page_nums[i]).css("visibility", "hidden");
                    }
                }
            } else {
                $(btn_next).css("visibility", "");
            }

            if (numberOfPois <= 10) {
                console.log(LOG_HEAD, domObj);
                if (currentBtnIndex == 0 && pageIndex == 0) {
                    $(domObj).css("display", "none");
                }
            } else if (numberOfPois <= 50) {
                $(btn_prev).css("visibility", "hidden");
                $(btn_next).css("visibility", "hidden");
                console.log(LOG_HEAD, "currentBtnIndexLimit " + currentBtnIndexLimit);
                for (var i = currentBtnIndexLimit; i < btn_page_nums.length; i++) {
                    $(btn_page_nums[i]).css("visibility", "hidden");
                }
            }

        }

        //선택된 번호를 진하게 강조
        var highlightPageNum = function(index) {
            for (var i = 0; i < btn_page_nums.length; i++) {
                $(btn_page_nums[i]).attr("class", "");
            }
            $(btn_page_nums[index]).attr("class", "on");
        }

        //페이지 번호 변경
        var setPage = function(index) {
            currentBtnIndex = 0;
            pageIndex = index || 0;
            for (var i = 0; i < btn_page_nums.length; i++) {
                $(btn_page_nums[i]).children("a").text((i + 1) + (pageIndex * PAGE_BTN_COUNT));
            }
            highlightPageNum(0);
        };

        //객체 생성시 자동 실행
        var bindBtnClicked = function() {
            console.log(LOG_HEAD + "click event\n", $(btn_prev).click());
            btn_prev = domObj.children("button")[0];
            btn_next = domObj.children("button")[1];
            btn_page_nums = domObj.children("span").children("button");
            //bind btn_prev btn_next
            $(btn_prev).attr("onclick", "").unbind("click");
            $(btn_prev).on("click", function() {
                console.log(LOG_HEAD + "btn_prev_clicked");
                changePageNum(false);
            });
            $(btn_next).attr("onclick", "").unbind("click");
            $(btn_next).on("click", function() {
                console.log(LOG_HEAD + "btn_next_clicked");
                changePageNum(true);
            });
            //bind page number button
            for (var i = 0; i < btn_page_nums.length; i++) {
                $(btn_page_nums[i]).attr("onclick", "").unbind("click");
                $(btn_page_nums[i]).on("click", function() {
                    //console.log(LOG_HEAD + "btn_page_nums_" + $(this).index() + "clicked");
                    console.log(LOG_HEAD + "paginationId\n", paginationId);
                    currentBtnIndex = $(this).index();
                    highlightPageNum(currentBtnIndex);
                    //검색 start
                    searchOpt.pageIndex = pageIndex;
                    searchOpt.currentBtnIndex = currentBtnIndex;
                    searchFunc(searchOpt);
                });
            }
            setPage(0);
        };

        //next or prev 눌렀을 경우
        var changePageNum = function(isNext) {
            if (isNext) {
                pageIndex++;
                searchOpt.pageIndex = pageIndex;
                searchOpt.currentBtnIndex = 0;
                searchFunc(searchOpt);
                setPage(pageIndex);
                highlightPageNum(0);
            } else {
                pageIndex--;
                searchOpt.pageIndex = pageIndex;
                searchOpt.currentBtnIndex = 4;
                searchFunc(searchOpt);
                setPage(pageIndex);
                highlightPageNum(4);
            }

            setPaginationUI();
        };

        function init() {
            paginationId++;
            return {
                setDomObj: setDomObj,
                setSearchFunc: setSearchFunc,
                setSearchOpt: setSearchOpt,
                bindBtnClicked: bindBtnClicked,
                setNumberOfPois: setNumberOfPois,
                getDomObj: getDomObj
            }
        }

        return {
            getInstance: function() {
                if (instance === undefined) {
                    console.log(LOG_HEAD + "Pagination\n", "init");
                    instance = init();
                }
                return instance;
            }
        }
    })();
    //End Pagination

    function init() {
        //singleton 객체 정의
        return {
            //public
            getSearchOpt: getSearchOpt,
            setSearchOpt: setSearchOpt,
            getPlaceSearchOpt: getPlaceSearchOpt,
            setPlaceSearchOpt: setPlaceSearchOpt,
            autoComplete: autoComplete,
            searchPOI: searchPOI,
            searchInRoute: searchInRoute,
            searchCategoryPlace: searchCategoryPlace,
            reverseGeocode: reverseGeocode,

            newPaginationInstance: function() {
                return Pagination.getInstance();
            }
        }
    }

    return {
        getInstance: function() {
            if (instance === undefined) {
                console.log("init");
                instance = init();
            }
            return instance;
        }
    }
})();
